<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>EsferaBR - Sistema de Retirada de Kit</title>
<link rel="stylesheet" href="{base_url}assets/css/style.default.css" type="text/css" />
<script type="text/javascript" src="{base_url}assets/js/plugins/jquery-1.7.min.js"></script>
<link rel="shortcut icon" href="{base_url}assets/images/logo.ico" type="image/x-icon"/>
<!--[if IE 9]>
    <link rel="stylesheet" media="screen" href="css/style.ie9.css"/>
<![endif]-->
<!--[if IE 8]>
    <link rel="stylesheet" media="screen" href="css/style.ie8.css"/>
<![endif]-->
<!--[if lt IE 9]>
	<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<![endif]-->
</head>

<body class="loginpage">

	<div class="loginbox">
    	<div class="loginboxinner">
        	
            <div class="logo">
            	<h1 style="padding-bottom: 10px;"><img src="{base_url}assets/images/logo_escrito_branco.png" style="width: 300px;" /></h1>
                <p>Sistema de Retirada de Kits</p>
            </div><!--logo-->
            
            <br clear="all" />
			
            <div class="nousername"{ERROR}>
				<div class="loginmsg">Login ou senha incorreto.</div>
            </div><!--nousername-->
			
            <form id="login" action="{base_url}login/logar" method="post">
            	<input type="hidden" name="TIPO_LOGIN" value="{TIPO_LOGIN}" />
                <div class="username">
                	<div class="usernameinner">
                    	<input type="text" name="username" id="username" placeholder="Login" required="required" />
                    </div>
                </div>
                
                <div class="password">
                	<div class="passwordinner">
                    	<input type="password" name="password" id="password" placeholder="Senha" required="required" />
                    </div>
                </div>
                
                <button>Entrar</button>
				
            </form>
            
        </div><!--loginboxinner-->
    </div><!--loginbox-->

</body>
</html>