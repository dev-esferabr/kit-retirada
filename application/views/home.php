	<div class="pageheader">
		<h1 class="pagetitle">Painel</h1>
		<span class="pagedesc">Sistema do central de Retiradas de Kits da EsferaBR.</span>
		
		<ul class="hornav" style="height: 1px;">&nbsp;</ul>
	</div><!--pageheader-->
    
    <div class="contentwrapper">
        
        <div id="contentwrapper" class="contentwrapper">
			<div id="entrega_estatistica_legenda"></div>
			<canvas id="entrega_estatistica" style="height: 200px; width: 100%;"></canvas>
			<script>
				var randomScalingFactor = function(){ return Math.round(Math.random()*10000)};
				var barChartData = {
					labels : [{ULTIMOS7}'{nome}',{/ULTIMOS7}],
					datasets : [
						{
							label: "Entregue",
							fillColor : "rgba(151,187,205,0.5)",
							strokeColor : "rgba(151,187,205,0.8)",
							highlightFill : "rgba(151,187,205,0.75)",
							highlightStroke : "rgba(151,187,205,1)",
							data : [{ULTIMOS7}'{entregue}',{/ULTIMOS7}]
						},
						{
							label: "Aguardando",
							fillColor : "rgba(219,109,109,0.5)",
							strokeColor : "rgba(219,109,109,0.8)",
							highlightFill : "rgba(219,109,109,0.75)",
							highlightStroke : "rgba(219,109,109,1)",
							data : [{ULTIMOS7}'{sobrou}',{/ULTIMOS7}]
						}
					]
			
				}
				window.onload = function(){
					var ctx = document.getElementById("entrega_estatistica").getContext("2d");
					window.myBar = new Chart(ctx).Bar(barChartData, {
						responsive : false,
						scaleLineColor : "rgba(0,0,0,0.3)", 
						scaleLineWidth : 1, 
						scaleShowLabels : true,
						scaleFontSize : 12, 
						scaleFontStyle : "normal", 
						scaleFontColor : "#000", 
						scaleShowGridLines : true,
						scaleGridLineColor : "rgba(0,0,0,.05)",
						
						barDatasetSpacing : 0,
						barValueSpacing : 20,
					})
					
					jQuery('#entrega_estatistica_legenda').html(window.myBar.generateLegend());
				}
			</script>
			<style>
				#entrega_estatistica_legenda {
					position: relative;
					text-align: center;
					right: 10px;
				}
				
				#entrega_estatistica_legenda li {
					display: inline-block;
					margin-right: 10px;
					margin-left: 10px;
				}
				
				#entrega_estatistica_legenda span {
					margin-right: 5px;
					margin-top: 5px;
					height: 10px;
					float: left;
					width: 10px;
				}
			</style>
        	<br />&nbsp;<br />
			<div class="two_third dashboard_left">
				<div id="calendar" style="width: 92%; margin-left: 4%;">&nbsp;</div>
				<script>
					/* initialize the calendar */
					jQuery('#calendar').fullCalendar({
						header: {
							left: '',
							center: 'title',
							right: 'today, prev, next'
						},
						buttonText: {
							prev: '&laquo;',
							next: '&raquo;',
							prevYear: '&nbsp;&lt;&lt;&nbsp;',
							nextYear: '&nbsp;&gt;&gt;&nbsp;',
							today: 'HOJE',
							month: 'month',
							week: 'week',
							day: 'day'
						},
						editable: false,
						droppable: false,
						events: [	{CALENDARIO}{
										title: '{nome}',
										start: '{dtevento}'
									},{/CALENDARIO}
								]
					});
				</script>
			</div>
            <div class="one_third last dashboard_right">
				<div class="widgetbox">
					<div class="title"><h3>Ultimos logins</h3></div>
					<div class="widgetoptions">
						<div class="right"><a href="{base_url}funcionario">Ver todos os usuarios</a></div>
						<a href="{base_url}funcionario/novo">Novo usuario</a>
					</div>
					<div class="widgetcontent userlistwidget nopadding">
						<ul>{ULTIMOS_LOGINS}
							<li>
								<div class="avatar"><img src="{avatar}" width="50" /></div>
								<div class="info">
									<a href="<?php echo base_url(); ?>funcionario/{cod_funcionario}">{nome}</a> <br />
									&nbsp;<br />
									{data}
								</div><!--info-->
							</li>{/ULTIMOS_LOGINS}
						</ul>
					</div><!--widgetcontent-->
				</div>
			</div>
            
            <div id="activities" class="subcontent" style="display: none;">
            	&nbsp;
            </div><!-- #activities -->
        
        </div><!--contentwrapper-->
        
        <br clear="all" />
        
	</div><!-- centercontent -->