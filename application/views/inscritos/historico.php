<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Quem retirou</title>
<link rel="stylesheet" href="{base_url}assets/css/style.default.css" type="text/css" />
<script type="text/javascript" src="{base_url}assets/js/plugins/jquery-1.7.min.js"></script>
<script type="text/javascript" src="{base_url}assets/js/plugins/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="{base_url}assets/js/plugins/jquery.cookie.js"></script>
<script type="text/javascript" src="{base_url}assets/js/custom/general.js"></script>
<!--[if IE 9]>
    <link rel="stylesheet" media="screen" href="{base_url}assets/css/style.ie9.css"/>
<![endif]-->
<!--[if IE 8]>
    <link rel="stylesheet" media="screen" href="{base_url}assets/css/style.ie8.css"/>
<![endif]-->
<!--[if lt IE 9]>
	<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<![endif]-->
</head>

<body>

<div>
    <div class="contentwrapper padding10">
    	<div class="errorwrapper error404">
        	<div class="errorcontent">
                <h1>Quem retirou</h1>
				<form action="#" method="post" class="form_retirada">
					<table cellpadding="0" cellspacing="0" class="table invoicefor" style="height: 100%;">
						<tbody>
							<tr>
								<td width="20%" style="vertical-align: middle;"><strong>Nome:</strong></td>
								<td width="80%">{nome}</td>
							</tr>
							<tr>
								<td style="vertical-align: middle;"><strong>Telefone:</strong></td>
								<td>{telefone}</td>
							</tr>
							<tr>
								<td style="vertical-align: middle;"><strong>Data:</strong></td>
								<td>{dt_alterado}</td>
							</tr>
							<tr>
								<td style="vertical-align: top;"><strong>Observação:</strong></td>
								<td>{obs}</td>
							</tr>
						</tbody>
					</table>
				</form>
            </div><!--errorcontent-->
        </div><!--errorwrapper-->
    </div>    
    
    
</div><!--bodywrapper-->

</body>
</html>
