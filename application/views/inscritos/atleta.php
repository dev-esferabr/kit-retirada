<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Informações do atleta</title>
<link rel="stylesheet" href="{base_url}assets/css/style.default.css" type="text/css" />
<script type="text/javascript" src="{base_url}assets/js/plugins/jquery-1.7.min.js"></script>
<script type="text/javascript" src="{base_url}assets/js/plugins/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="{base_url}assets/js/plugins/jquery.cookie.js"></script>
<script type="text/javascript" src="{base_url}assets/js/custom/general.js"></script>
<script type="text/javascript" src="{base_url}assets/js/plugins/jquery.mask.js"></script>
<!--[if IE 9]>
    <link rel="stylesheet" media="screen" href="css/style.ie9.css"/>
<![endif]-->
<!--[if IE 8]>
    <link rel="stylesheet" media="screen" href="css/style.ie8.css"/>
<![endif]-->
<!--[if lt IE 9]>
	<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<![endif]-->

<script>
    jQuery(document).ready(function(){
        jQuery('.editarUsuario > .edit').click(function(){
            jQuery('.editarUsuario input.editar, .editarUsuario select.editar').css('display','block');
            jQuery('.editarUsuario td span.editar').css('display','none');
            jQuery('.editarUsuario input[type=submit]').css('display','block');
        });
        jQuery('.editarUsuario > .new').click(function(){
            jQuery('.editarUsuario > .edit').css('display','none')
            jQuery('.editarUsuario input[type=text]').val('');
            jQuery('.editarUsuario input.usuario').val('');
            jQuery('.editarUsuario input, .editarUsuario select').css('display','block');
            jQuery('.editarUsuario td span').css('display','none');
        });
        jQuery('.editarUsuario > form').submit(function(){
            var valida = 1;
            
            if(jQuery("#telefone").val() == '' || jQuery("#celular").val() == '' || jQuery("#v_nr_documento").val() == '' || jQuery("#email").val() == '' || jQuery("#dtnascimento").val() == ''){
                valida = 0;
            }
            
            if(valida == 1){
                dados = jQuery(this).serialize();
                jQuery.ajax({
                    type: "POST",
                    url: '{base_url}inscritos/alterarusuario?'+dados,
                    dataType: 'html',
                    cache: false
                })
                .done(function(data){
                    alert("Alterado com sucesso!");
                    parent.location.reload();
                });
            }else{
                alert('Preencha todos os campos!');
            }
            return false;
        });
        
        jQuery('#telefone').mask("(99) 9999-9999");
        jQuery('#celular').mask("(99) 9999-9999?9").ready(function (event) {
            var target, phone, element;
            target = (event.currentTarget) ? event.currentTarget : event.srcElement;
            phone = target.value.replace(/\D/g, '');
            element = $(target);
            element.unmask();
            if (phone.length > 10) {
                element.mask("(99) 99999-999?9");
            } else {
                element.mask("(99) 9999-9999?9");
            }
        });

        jQuery('#v_nr_documento').blur(function () {
            if (!validarCPF(jQuery('#v_nr_documento').val())) {
                jQuery('#v_nr_documento').val('');
                alert('CPF não é valido.');
                return false;
            }
        });
        
        jQuery('#v_nr_documento').mask("999.999.999-99");
        jQuery('#dtnascimento').mask("99/99/9999");

        jQuery('#email').blur(function () {
            if (!verificaEMAIL(jQuery('#email').val())) {
                jQuery('#email').val('');
                alert('E-mail invalido.');
                return false;
            }
        });
    });

    function addMaskDocument() {
        jQuery('#v_nr_documento').mask(jQuery('#h_tipo_cpf option:selected').attr('data-mask'));
    }

    function validarCPF(cpf) {
        cpf = cpf.replace(/[^\d]+/g, '');
        if (cpf == '')
            return false;
        // Elimina CPFs invalidos conhecidos    
        if (cpf.length != 11 ||
                cpf == "00000000000" ||
                cpf == "11111111111" ||
                cpf == "22222222222" ||
                cpf == "33333333333" ||
                cpf == "44444444444" ||
                cpf == "55555555555" ||
                cpf == "66666666666" ||
                cpf == "77777777777" ||
                cpf == "88888888888" ||
                cpf == "99999999999")
            return false;
        // Valida 1o digito 
        add = 0;
        for (i = 0; i < 9; i ++)
            add += parseInt(cpf.charAt(i)) * (10 - i);
        rev = 11 - (add % 11);
        if (rev == 10 || rev == 11)
            rev = 0;
        if (rev != parseInt(cpf.charAt(9)))
            return false;
        // Valida 2o digito 
        add = 0;
        for (i = 0; i < 10; i ++)
            add += parseInt(cpf.charAt(i)) * (11 - i);
        rev = 11 - (add % 11);
        if (rev == 10 || rev == 11)
            rev = 0;
        if (rev != parseInt(cpf.charAt(10)))
            return false;
        return true;
    }

    function verificaEMAIL(email) {
        var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        return regex.test(email);
    }
</script>

</head>

<body>

<div>
    <div class="contentwrapper padding10">
    	<div class="errorwrapper error404">
        	<div class="errorcontent editarUsuario">
                <h1>Dados do atleta</h1>
                    <a href="javascript:void(0);" class="edit voltar">Editar</a>
                    <a href="javascript:void(0);" class="new voltar">Novo</a>
				<form action="#" method="post" class="form_retirada">
                                    <input type="hidden" class="usuario" name="cod_usuario" value="{cod_usuario}">
                                    <input type="hidden" name="cod_inscritos" value="{cod_inscritos}">
					<table cellpadding="0" cellspacing="0" class="table invoicefor" style="width: 50%; float: left;">
						<tbody>
							<tr>
								<td width="20%"><strong>Nome:</strong></td>
                                                                <td width="80%">
                                                                    <span class="editar">{nome}</span>
                                                                    <input type="text" class="editar" value="{nome}" name="nome">
                                                                </td>
							</tr>
							<tr>
								<td><strong>Documento:</strong></td>
								<td>
                                                                    <span>{documento}</span>
                                                                    <input type="text" value="" name="documento" id="v_nr_documento">
                                                                </td>
							</tr>
							<tr>
								<td><strong>Nascimento:</strong></td>
                                                                <td width="80%">
                                                                    <span class="editar">{dtnascimento}</span>
                                                                    <input type="text" class="editar" value="{dtnascimento}" name="dtnascimento" id="dtnascimento">
                                                                </td>
							</tr>
							<tr>
								<td><strong>Necessidades:</strong>
                                                                <td width="80%">
                                                                    <span class="editar">{necessidades}</span>
                                                                    <select name="necessidades" class="editar" style="display: none;">
                                                                        <option value="N">Não</option>
                                                                        <option value="S">Sim</option>
                                                                    </select>
                                                                </td>
							</tr>
                                                        <tr class="novo">
								<td><strong>Sexo:</strong></td>
                                                                <td>
                                                                    <span class="editar">{genero}</span>
                                                                    <select name="genero" class="editar" style="display: none;">
                                                                        <option value="M">Masculino</option>
                                                                        <option value="F">Feminino</option>
                                                                    </select>
                                                                </td>
							</tr>
							<tr>
								<td><strong>Telefone:</strong></td>
                                                                <td width="80%">
                                                                    <span class="editar">{telefone}</span>
                                                                    <input type="text" class="editar" value="{telefone}" name="telefone" id="telefone">
                                                                </td>
							</tr>
							<tr>
								<td><strong>Celular:</strong></td>
                                                                <td width="80%">
                                                                    <span class="editar">{celular}</span>
                                                                    <input class="editar" type="text" value="{celular}" name="celular" id="celular">
                                                                </td>
							</tr>
						</tbody>
					</table>
					<table cellpadding="0" cellspacing="0" class="table invoicefor" style="width: 50%; float: left;">
						<tbody>
							<tr>
								<td><strong>E-mail:</strong></td>
								<td>
                                                                    <span>{email}</span>
                                                                    <input type="text" value="" name="email" id="email">
                                                                </td>
							</tr>
							<tr>
								<td width="20%"><strong>Modalidade:</strong></td>
								<td width="80%">{modalidade}</td>
							</tr>
							<tr>
								<td><strong>Categoria:</strong></td>
								<td>{categoria}</td>
							</tr>
							<tr>
								<td><strong>Nº de peito:</strong></td>
								<td>{nm_peito}</td>
							</tr>
							<tr>
								<td><strong>Camiseta:</strong></td>
								<td>{camiseta}</td>
							</tr>
							<tr>
								<td><strong>Equipe:</strong></td>
								<td>{equipe}</td>
							</tr>
						</tbody>
					</table>
                                    <input type="submit" value="Alterar Usuário" style="display: none; float: right;">
				</form>
				<br clear="all" />
            </div><!--errorcontent-->
        </div><!--errorwrapper-->
    </div>    
    
    
</div><!--bodywrapper-->

</body>
</html>
