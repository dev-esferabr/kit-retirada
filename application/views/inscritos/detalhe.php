<div class="pageheader">
		<h1 class="pagetitle">Pedido</h1>
		<span class="pagedesc">Informação sobre o pedido de protocolo {ID}.</span>
		
		<ul class="hornav">
			<li class="current"><a href="#event_new">Pedido</a></li>
			<li><a href="#event_old">Histórico</a></li>
		</ul>
	</div><!--pageheader-->
    
    <div id="contentwrapper" class="contentwrapper">
        <form class="stdform" id="form_pedido">
			<div id="event_new" class="subcontent">
				<div class="contenttitle2 nomargintop">
					<h3>Dados da inscrição</h3>
				</div>
				<br clear="all" />
				<div class="one_half">
					<table cellpadding="0" cellspacing="0" class="table invoicefor">
						<tbody>
							<tr>
								<td width="30%">Protocolo:</td>
								<td width="70%">{ID}</td>
							</tr>
							<tr>
								<td>Data do pedido:</td>
								<td>{DT_PEDIDO}</td>
							</tr>
							<tr>
								<td>Status:</td>
								<td>{STATUS}</td>
							</tr>
						</tbody>
					</table>
				</div>
				<div class="one_half last">
					<table cellpadding="0" cellspacing="0" class="table invoicefor">
						<tbody>
							<tr>
								<td width="30%">Forma de pagamento:</td>
								<td width="70%">{PAGAMENTO}</td>
							</tr>
							<tr>
								<td>Comprador:</td>
								<td>{COMPRADOR}</td>
							</tr>
							<tr>
								<td>Data de pagamento:</td>
								<td>{DT_PAGAMENTO}</td>
							</tr>
						</tbody>
					</table>
				</div>
				<br clear="all" />
				
				<div class="contenttitle2">
					<h3>Altetas Inscritos</h3>
				</div>
				<table cellpadding="0" cellspacing="0" border="0" class="stdtable atletas" id="dyntable">
					<thead>
						<tr>
							<th class="head0">Nome</th>
							<th class="head1">Número de peito</th>
							<th class="head0">Modalidade</th>
							<th class="head1">Categoria</th>
							<th class="head0">Camiseta</th>
                                                        <th class="head1">Capitão</th>
							<th class="head0">Retirado</th>
							<th class="head1">Retirar</th>
						</tr>
					</thead>
					<tbody>{ATLETAS}
                                            <tr data-id_usuario="{cod_usuario}" data-cod_inscritos="{cod_inscritos}">
							<td>
								<?php if(strlen($ID) > 10) { ?>
									{nome}
								<?php } else { ?>
									<a href="#" class="dados_atleta" data-cod_inscritos="{cod_inscritos}" data-id="{cod_inscritos}">{nome}</a>
								<?php } ?>
							</td>
							<td>{nm_peito}</td>
                                                        <td>
                                                            <?php if($NIVEL_ADMIN > 0):?>
                                                            <select name="modalidade" class="modalidade" required="required">
                                                                <option value="">Escolha</option>
                                                                {MODALIDADE}<option value="{cod_modalidade}" {selected}>{nome}</option>{/MODALIDADE}
                                                            </select>
                                                            <?php else:?>
                                                            {modalidade}
                                                            <?php endif;?>
                                                        </td>
							<td>
                                                            <?php if($NIVEL_ADMIN > 0):?>
                                                            <select name="categoria" class="categoria" required="required">
                                                                <option class="visivel" value="">Escolha</option>
                                                                {CATEGORIA}<option value="{cod_categoria}" data-modalidade="{id_modalidade}" {selected}>{nome}</option>{/CATEGORIA}
                                                            </select>
                                                            <?php else:?>
                                                            {categoria}
                                                            <?php endif;?>
                                                        </td>
							<td>{camiseta}</td>
                                                        <td>{fl_capitao}</td>
							<td><a href="#" class="retirado_sim" data-id="{cod_inscritos}">{retirado}</a></td>
							<td>
                                                            <?php if($id_pedido_status == 2 || $NIVEL_ADMIN == 1){?>
								<?php if(strlen($ID) > 10) {?>
								<input type="checkbox" name="retirada_incricoes[ok]" checked="checked" disabled="disabled" />
								<?php } else {
                                                                    if($NIVEL_ADMIN == 0 && $retirado){?>
                                                                <input type="checkbox" name="retirada_incricoes[{cod_inscritos}]" value="1"{retiradoCheck} {disabled} />
                                                                <?php }else{?>
                                                                        <input type="checkbox" name="retirada_incricoes[{cod_inscritos}]" value="1"{retiradoCheck} />
                                                                    <?php }
                                                                } 
                                                            }?>
							</td>
						</tr>{/ATLETAS}
					</tbody>
				</table>
				<a href="#" style="color: blue;" onclick="jQuery('input[name^=retirada_incricoes]').prop('checked', true); return false;">Marcar todos</a> |
				<a href="#" style="color: blue;" onclick="jQuery('input[name^=retirada_incricoes]').prop('checked', false); return false;">Desmarcar todos</a>
				<br clear="all" />
				
				<div class="contenttitle2">
					<h3>Produtos comprados no carrinho</h3>
				</div>
				<table cellpadding="0" cellspacing="0" border="0" class="stdtable" id="dyntable">
					<thead>
						<tr>
							<th class="head0">Produto</th>
							<th class="head0">Caracteristica 1</th>
							<th class="head0">Caracteristica 2</th>
							<th class="head0">Retirado</th>
							<th class="head0">Retirar</th>
						</tr>
					</thead>
					<tbody>
					<?php if(count($PRODUTOS) > 0) { ?>
						{PRODUTOS}
							<tr class="gradeX">
								<td>{ds_titulo}</td>
								<td>{ds_recurso}</td>
								<td>{ds_recurso2}</td>
								<td><a href="#" class="retirado_sim" data-id="0/{cod_pedido_produto}">{retirado}</a></td>
								<td><input type="checkbox" name="retirada_produtos[{cod_pedido_produto}]" value="1"{retiradoCheck} /></td>
							</tr>
						{/PRODUTOS}
					<?php } else { ?>
							<tr class="gradeX">
								<td colspan="5" style="text-align: center;">Nenhum produto</td>
							</tr>
					<?php } ?>
					</tbody>
				</table>
				<a href="#" style="color: blue;" onclick="jQuery('input[name^=retirada_produtos]').prop('checked', true); return false;">Marcar todos</a> |
				<a href="#" style="color: blue;" onclick="jQuery('input[name^=retirada_produtos]').prop('checked', false); return false;">Desmarcar todos</a>
				<br clear="all" />
				&nbsp;
				<br />
                                <a href="<?php echo $base_url; ?>inscritos/" class="voltar" style="float: left;">Voltar</a>
				<input type="submit" class="submit radius2" value="Retirar" style="float: right;">
                                <?php if($NIVEL_ADMIN > 0):?>
                                <a href="javascript:void(0);" class="voltar" id="alterar" style="float: right;">Alterar</a>
                                <?php endif;?>
			
			</div><!--contentwrapper-->
		</form>
		
        <div id="event_old" class="subcontent">
			<table cellpadding="0" cellspacing="0" border="0" class="stdtable" id="table_inscritos">
				<thead>
					<tr>
						<th class="head1">Data</th>
						<th class="head0">Funcionario</th>
						<th class="head1">Quem Retirou</th>
						<th class="head0">Telefone</th>
						<th class="head1">Observação</th>
						<th class="head0">Ação</th>
					</tr>
				</thead>
				<tbody>{HISTORICO}
					<tr>
						<td>{dt_alterado}</td>
						<td>{funcionario}</td>
						<td>{nome}</td>
						<td>{telefone}</td>
						<td><pre>{obs}</pre></td>
						<td>{acao}</td>
					</tr>{/HISTORICO}
				</tbody>
			</table>
		</div>
        
        <br clear="all" />
        
	</div><!-- centercontent -->
<style>
	.stdtable td {
		vertical-align: top;
	}
</style>
<div id="retirada">
	<div class="contenttitle2">
		<h3>Retirada do Kit</h3>
	</div>
	<br clear="all" />
	<form action="{base_url}inscritos/retirado/{ID_EVENTO}/{ID}" method="post" class="form_retirada">
		<label><input type="checkbox" name="eh_comprador" class="eh_comprador" checked="checked" value="{ID_COMPRADOR}" /> O comprador que está retirando o Kit.</label>
		<br clear="all" />
		<table cellpadding="0" cellspacing="0" class="table invoicefor" style="height: 100%;">
			<tbody>
				<tr>
					<td width="20%" style="vertical-align: middle;"><strong>Nome:</strong></td>
					<td width="80%"><span>{COMPRADOR}</span><input type="text" name="nome_retirado" class="nome_retirado" value="{COMPRADOR}" data-inicial="{COMPRADOR}" /></td>
				</tr>
				<tr>
					<td style="vertical-align: middle;"><strong>Telefone:</strong></td>
					<td><span>{TELEFONE}</span><input type="text" name="telefone_retirado" class="telefone_retirado" value="{TELEFONE}" data-inicial="{TELEFONE}" /></td>
				</tr>
				<tr>
					<td style="vertical-align: top;"><strong>Observação:</strong></td>
					<td><textarea name="obs"></textarea></td>
				</tr>
			</tbody>
		</table>
		<div class="inputs_hidden"></div>
		<br clear="all" />
                <a href="<?php echo $base_url; ?>inscritos/" class="buttonFinish" style="float: left;">Voltar</a>
		<input type="submit" class="submit radius2" value="Retirar" style="float: right;">
	</form>
</div>

<script>
	jQuery(document).ready(function(e) {
                jQuery(".modalidade").change(function(){
                    modali = jQuery(this).find("option:selected").val();
                    jQuery(this).parents('tr').find(".categoria option").css("display","none");
                    jQuery(this).parents('tr').find(".categoria .visivel").css('display','block');
                    jQuery(this).parents('tr').find(".categoria option").each(function(){
                        if(jQuery(this).data("modalidade") == modali){
                            jQuery(this).css("display","block")
                        }
                    });
                    jQuery(this).parents('tr').find(".categoria .visivel").attr('selected','selected');
                });
                
                jQuery(".atletas").find('tr').find(".categoria option").each(function(){
                    modali = jQuery(this).parents('tr').find(".modalidade option:selected").val();
                    jQuery(this).parents('tr').find(".categoria option").css("display","none");
                    jQuery(this).parents('tr').find(".categoria .visivel").css('display','block');
                    jQuery(this).parents('tr').find(".categoria option").each(function(){
                        if(jQuery(this).data("modalidade") == modali){
                            jQuery(this).css("display","block")
                        }
                    });
                });
                
                jQuery('#alterar').click(function() {
                    var arrayObj = new Array();
                    jQuery(".atletas tbody tr").each(function(){
                       var obj= new Object();
                       obj.cod_inscritos = jQuery(this).data('cod_inscritos');
                       obj.id_usuario = jQuery(this).data('id_usuario');
                       obj.id_modalidade = jQuery(this).find('.modalidade option:selected').val();
                       obj.id_categoria = jQuery(this).find('.categoria option:selected').val();
                       arrayObj .push(obj);
                    });
                    var jsonObjs = JSON.stringify(arrayObj );
                    
                    jQuery.ajax({
                        type: "POST",
                        url: '{base_url}inscritos/alterar',
                        data: { 'dados':jsonObjs},
                        dataType: 'html',
                        cache: false
                    })
                    .done(function(data){
                        alert('ok');
                    });
                });
		jQuery('#form_pedido').submit(function() {
			<?php if(strlen($ID) > 10) { ?>
				document.location = '{base_url}inscritos';
				return false;
			<?php } ?>
			// Abrir LightBox
			jQuery.fancybox({
				maxWidth	: 800,
				maxHeight	: 600,
				fitToView	: false,
				width		: '70%',
				height		: '70%',
				autoSize	: false,
				closeClick	: false,
				openEffect	: 'none',
				closeEffect	: 'none',
				content : jQuery('#retirada').html()
			});
			
			// Adicionar Click do EhComprador
			jQuery('.fancybox-outer .form_retirada .eh_comprador').click(function() {
				if(jQuery(this).is(':checked')) {
					jQuery('.fancybox-outer .form_retirada').removeClass('nao_comprador');
					jQuery('.fancybox-outer .form_retirada .nome_retirado').val(jQuery('.fancybox-outer .form_retirada .nome_retirado').attr('data-inicial'));
					jQuery('.fancybox-outer .form_retirada .telefone_retirado').val(jQuery('.fancybox-outer .form_retirada .telefone_retirado').attr('data-inicial'));
				} else {
					jQuery('.fancybox-outer .form_retirada').addClass('nao_comprador');
					jQuery('.fancybox-outer .form_retirada .nome_retirado').val('');
					jQuery('.fancybox-outer .form_retirada .telefone_retirado').val('');
				}
			});
			
			// Adicionar Inputs ao LightBox
			jQuery('#form_pedido input').each(function(index, element) {
				if(jQuery(this).attr('type') == 'checkbox') {
					var html = '<input type="hidden" name="' + jQuery(this).attr('name') + '"';
					html += (jQuery(this).is(':checked')? ' value="1"': ' value="0"') + '>';
					
					jQuery('.fancybox-outer .form_retirada .inputs_hidden').append(html);
				}
			});
			
			return false;
		});
		triggerRetiradoSim();
	});
</script>
