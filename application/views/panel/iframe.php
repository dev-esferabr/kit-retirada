<style type="text/css"> * { height: 100%; width: 100%; } </style>
<iframe src="<?php echo base_url() . 'panel/carregando'; ?>" id="main_iframe"></iframe>
<?php if($NIVEL == 1) { ?>
	<div id="virar_server">
		Transformar este computador em servidor ? &nbsp;
		<a class="sim" href="#" style="color: green;">Sim</a> - <a class="nao" href="#" style="color: red;">Não</a>
	</div>
<?php } ?>
<script type="text/javascript">
	var isServer = <?php echo (($thisServer)? 'true': 'false'); ?>;
	var servidor = <?php echo (($thisServer)? '"localhost"': 'false'); ?>;
	jQuery(document).ready(function(e) {
		if(!isServer) {
			procurarServer();
		} else {
			jQuery('#virar_server').remove();
			abrirIframe(true);
			
			// Verificar outro server ligado na rede
			setInterval(function() {
				//verificarOutroServer();
			}, 60000);
		}
		
		// Clicks de transformar esta estação em servidor
		jQuery('#virar_server .sim').click(function() {
			abrirIframeUrl(base_url + 'sync/central');
			return false;
		});
		jQuery('#virar_server .nao').click(function() {
			jQuery('#virar_server').remove();
			return false;
		});
		
		// Ficar verificando se o server está online
		serverOn();
		
		// Verificar se a internet está ligada
		<?php if($NIVEL == 1) { ?>
			setInterval(checkNetOnline, 30000);
		<?php } ?>
	});
	
	// Procurar servidor no modo Rapido
	function procurarServer() {
		jQuery.get( base_url + 'sync/findFast/', function( data ) {
			if(data == 'false')
				procurarServer();
			else {
				servidor = data;
				abrirIframe(true);
				carregarDadosFullServer();
				
				// Sincronizar estação com o servidor
				setInterval(function() {
					carregarDadosServer();
				}, 20000);
				
				// Sincronizar servidor com a estação
				setTimeout(function() {
					servidorSincronizarEstacao();
				}, 30000);
			}
		});
	}
	
	// Verificar outro server ligado
	function verificarOutroServer() {
		jQuery.get( base_url + 'sync/findFast/?unico=true', function( data ) {
			if(data != 'false') {
				abrirIframeUrl(base_url + 'sync/duplicado');
			}
		});
	}
	
	// Abrir Iframe
	function abrirIframe() {
		abrirIframeUrl('http://' + servidor + '<?php echo $pastaBase; ?>/login/{NIVEL}');
	}
	
	// Abrir URL no Iframe
	function abrirIframeUrl(url) {
		jQuery('#main_iframe').attr('src', url);
	}
	
	// Recarregar Janela
	function pm_kit_reload() {
		document.location.reload();
	}
	
	// Fechar Janela
	function pm_kit_close() {
		document.location = '#closekiosk';
	}
	
	// Verificar se o server ainda está online
	function serverOn() {
		// Verificar se ainda está procurando o servidor
		if(servidor == false) {
			setTimeout(serverOn, 10000);
			return false;
		}
		
		// Verificar se o servidor está ON
		jQuery.ajax({
			type: 'GET',
			url: 'http://' + servidor + '<?php echo $pastaBase; ?>/sync/isServer',
			contentType: "application/json",
			dataType: 'jsonp',
			success: function(json) {
				if(json != true)
					document.location.reload();
				else
					setTimeout(serverOn, 10000);
			},
			error: function(e) {return false;
				document.location.reload();
			}
		});
	}
	
	// Carregar dados full do servidor
	function carregarDadosFullServer() {
		carregarDados(true);
	}
	
	// Sincronizar servidor com a estação
	function carregarDadosServer() {
		carregarDados(false);
	}
	
	// Sincronizar servidor com a estação
	function carregarDados(full) {
		jQuery.ajax({
			type: 'GET',
			url: '<?php echo $pastaBase; ?>/sync/sincronizar/' + servidor,
			success: function(data) {
			}
		});
	}
	
	// Ficar verificando se a internet está ligada
	function checkNetOnline() {
		/*var teste = new Image(); 
                teste.src = "http://static.ativo.com/wp-content/uploads/2014/05/logo-header.png?cache=" + new Date().getTime();
		jQuery(teste).bind('error', function () {
			avisarServerOnOff(false);
			teste = null;
		});
		jQuery(teste).one('load', function() {
			avisarServerOnOff(true);
			teste = null;
		});*/
                avisarServerOnOff(true);
	}
	
	function avisarServerOnOff(on) {
		jQuery.ajax({
			type: 'GET',
			url: '<?php echo $pastaBase; ?>/sync/on_off/' + ((on)? 'S': 'N'),
			success: function(data) {
			}
		});
	}
	
	function servidorSincronizarEstacao() {
		jQuery.ajax({
			type: 'GET',
			url: 'http://' + servidor + '<?php echo $pastaBase; ?>/sync/sincronizeme',
			success: function(data) {
			}
		});
	}
</script>
<style>
	#virar_server {
		border: 1px solid #DDD;
		background-color: #EEE;
		margin-left: -174px;
		position: absolute;
		margin-top: -26px;
		padding: 3px;
		height: 22px;
		width: auto;
		left: 50%;
		top: 100%;
		padding-left: 15px;
		border-bottom: 0px;
		padding-right: 15px;
		padding-bottom: 0px;
	}
</style>