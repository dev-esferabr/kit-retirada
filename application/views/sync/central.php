<div class="pageheader">
		<h1 class="pagetitle">Nova central</h1>
		<ul class="hornav" style="height: 1px;">&nbsp;</ul>
	</div><!--pageheader-->
    
    <div id="contentwrapper" class="contentwrapper">
        <div class="subcontent" id="nova_central" style="text-align: center;">
			<span class="fc-header-title"><h2>Deseja transformar está estação em uma central ?</h2></span>
			<div style="margin-top: 50px; font-size: 20px;">
				<a href="#" class="stdbtn btn_lime">Sim</a>
				<a href="#" class="stdbtn btn_red" style="background-image: none;">Não</a>
			</div>
		</div>
        <br clear="all" />
	</div><!-- centercontent -->
<script type="text/javascript">
jQuery(document).ready(function(e) {
	jQuery('#nova_central .btn_lime').click(function() {
		document.location = '{base_url}sync/nova_central';
	});
	jQuery('#nova_central .btn_red').click(function() {
		parent.postMessage("pm_kit_reload();","*");
	});
});
</script>