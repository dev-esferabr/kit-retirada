			<form class="stdform form_confirmar_senha" method="post" action="<?php echo $base_url; ?>funcionarios/<?php echo $ACAO; ?>">
				<div id="wizard" class="wizard">
					<div class="stepContainer">
						<div id="wiz1step1" class="formwiz content">
							<h4>Dados do atleta</h4>
							<p>
								<label>Nome</label>
								<span class="field">
									<input type="text" name="nome" id="nome" class="longinput" value="<?php echo $EDITAR[0]->nome; ?>" required="required" />
								</span>
							</p>
							<p>
								<label>E-mail</label>
								<span class="field">
									<input type="text" name="email" id="email" class="longinput" value="<?php echo $EDITAR[0]->email; ?>" required="required" />
								</span>
							</p>
							<p>
								<label>Login</label>
								<span class="field">
									<input type="text" name="login" id="login" class="longinput" value="<?php echo $EDITAR[0]->login; ?>" required="required" />
								</span>
							</p>
							<p>
								<label>Senha</label>
								<span class="field">
									<input type="password" name="senha" id="senha" class="longinput" />
								</span>
							</p>
							<p>
								<label>Confirmar senha</label>
								<span class="field">
									<input type="password" name="confirmar_senha" id="confirmar_senha" class="longinput" />
								</span>
							</p>
							<p>
								<label>Nivel</label>
								<span class="field">
									<select name="administrador" id="administrador">
										<option value="0"<?php if(@$EDITAR[0]->administrador == 0) { echo 'selected'; }?>>Normal</option>
										<option value="1"<?php if(@$EDITAR[0]->administrador == 1) { echo 'selected'; }?>>Administrador</option>
									</select>
								</span>
							</p>
							<p>
								<label>Ativo</label>
								<span class="field">
									<select name="ativo" id="ativo">
										<option value="1"<?php if(@$EDITAR[0]->ativo == 1) { echo 'selected'; }?>>Ativo</option>
										<option value="0"<?php if(@$EDITAR[0]->ativo === '0') { echo 'selected'; }?>>Bloqueado</option>
									</select>
								</span>
							</p>
						</div><!--#wiz1step2-->
					</div>
					<div class="actionBar">
                                            <a href="<?php echo $base_url; ?>funcionarios/" class="buttonFinish" style="float: left;">Voltar</a>
                                            <input type="submit" class="buttonFinish" value="Salvar" style="float: right;" />
					</div>
				</div><!--#wizard-->
			 </form>
<script>
	jQuery(document).ready(function(e) {
		jQuery('.form_confirmar_senha').submit(function() {
			if(jQuery('#senha').val() != jQuery('#confirmar_senha').val()) {
				alert('Senha diferente da confirmação de senha.');
				return false;
			}
			return true;
		});
	});
</script>