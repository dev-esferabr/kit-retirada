<script>
	jQuery(document).ready(function(e) {
		jQuery('#table_inscritos').dataTable({
			"sPaginationType": "full_numbers",
		});
	});
</script>
	<div class="pageheader">
		<h1 class="pagetitle">Funcionarios</h1>
		<span class="pagedesc">Administre a lista de funcionarios da Retirada de Kit.</span>
		
		<ul class="hornav">
			<li class="current"><a href="#event_new">Funcionarios</a></li>
			<li><a href="#event_old">Adicionar novo</a></li>
		</ul>
	</div><!--pageheader-->
    
    <div id="contentwrapper" class="contentwrapper">
        
        <div id="event_new" class="subcontent">
			<div class="contenttitle2">
				<h3>Funcionarios</h3>
			</div>
			<table cellpadding="0" cellspacing="0" border="0" class="stdtable" id="table_inscritos">
				<thead>
					<tr>
						<th class="head0">Nome</th>
						<th class="head1">Login</th>
						<th class="head0">Administrador</th>
						<th class="head0">Ativo</th>
						<th class="head1" style="width: 190px;">Ação</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th class="head0">Nome</th>
						<th class="head1">Login</th>
						<th class="head0">Administrador</th>
						<th class="head0">Ativo</th>
						<th class="head1">Ação</th>
					</tr>
				</tfoot>
				<tbody>{LIST}
					<tr>
						<td>{nome}</td>
						<td>{login}</td>
						<td>{administrador}</td>
						<td>{ativo}</td>
						<td>
							<a href="<?php echo base_url(); ?>funcionarios/editar/{cod_funcionario}" class="btn btn_yellow btn_trash"><span>Editar</span></a>
							&nbsp;&nbsp;
							<a href="<?php echo base_url(); ?>funcionarios/remover/{cod_funcionario}" class="btn btn_red btn_trash"><span style="color: #FFF;">Excluir</span></a>
						</td>
					</tr>{/LIST}
				</tbody>
			</table>
	    </div><!--contentwrapper-->
        <div id="event_old" class="subcontent">
			<?php include(dirname(__FILE__) . '/form.php'); ?>
		</div>
        <br clear="all" />
	</div><!-- centercontent -->