<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>EsferaBR - Sistema de Retirada de Kit</title>
<link rel="stylesheet" href="{base_url}assets/css/style.default.css" type="text/css" />
<script type="text/javascript" src="{base_url}assets/js/plugins/jquery-1.7.min.js"></script>
<script type="text/javascript" src="{base_url}assets/js/plugins/jquery-ui-1.8.16.custom.min.js"></script>
<script type="text/javascript" src="{base_url}assets/js/plugins/jquery.cookie.js"></script>
<script type="text/javascript" src="{base_url}assets/js/plugins/jquery.uniform.min.js"></script>
<script type="text/javascript" src="{base_url}assets/js/plugins/jquery.flot.min.js"></script>
<script type="text/javascript" src="{base_url}assets/js/plugins/jquery.flot.resize.min.js"></script>
<script type="text/javascript" src="{base_url}assets/js/plugins/Chart.min.js"></script>
<script type="text/javascript" src="{base_url}assets/js/plugins/jquery.slimscroll.js"></script>
<script type='text/javascript' src='{base_url}assets/js/plugins/fullcalendar.min.js'></script>
<script type="text/javascript" src="{base_url}assets/js/custom/general.js"></script>
<script type="text/javascript" src="{base_url}assets/js/plugins/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="{base_url}assets/js/plugins/jquery.dataTables.reload.min.js"></script>
<script type="text/javascript" src="{base_url}assets/js/plugins/fancybox/jquery.fancybox.js"></script>
<link rel="stylesheet" href="{base_url}assets/js/plugins/fancybox/jquery.fancybox.css" type="text/css" />
<link rel="shortcut icon" href="{base_url}assets/images/logo.ico" type="image/x-icon"/>
<!--[if lte IE 8]><script language="javascript" type="text/javascript" src="js/plugins/excanvas.min.js"></script><![endif]-->
<!--[if IE 9]>
    <link rel="stylesheet" media="screen" href="css/style.ie9.css"/>
<![endif]-->
<!--[if IE 8]>
    <link rel="stylesheet" media="screen" href="css/style.ie8.css"/>
<![endif]-->
<!--[if lt IE 9]>
	<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
<![endif]-->
<script type="text/javascript">
	var base_url = "<?php echo base_url(); ?>";
</script>
</head>
<body>
<div<?php if($displayHead) echo ' class="bodywrapper"'; ?>>