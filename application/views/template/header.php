<div class="topheader">
        <div class="left">
            <h1 class="logo"><img src="{base_url}assets/images/logo_escrito_branco.png" style="width: 140px;" alt="EsferaBR" /></h1>
            <br clear="all" />
        </div><!--left-->
        
        <div class="right">
            <div class="userinfo">
            	<img src="{login}{avatar}{/login}" alt="{login}{nome}{/login}" width="25" />
                <span>{login}{nome}{/login}</span>
            </div><!--userinfo-->
            
            <div class="userinfodrop">
            	<div class="avatar">
                	<a href="{base_url}perfil/editar"><img src="{login}{avatar}{/login}" alt="{login}{nome}{/login}" /></a>
                </div><!--avatar-->
                <div class="userdata">
                	<h4 style="width: 100%;">{login}{nome}{/login}</h4>
                    <span class="email">{login}{email}{/login}</span>
                    <ul>
                    	<!--<li><a href="{base_url}perfil/editar">Editar perfil</a></li>-->
                        <li><a href="{base_url}login/deslogar">Sair</a></li>
                    </ul>
                </div><!--userdata-->
            </div><!--userinfodrop-->
        </div><!--right-->
    </div><!--topheader-->
    
    <div class="header">{menu_current}
    	<ul class="headermenu">
		<?php if($ADMIN) { ?>
        	<li{home}><a href="<?php echo base_url(); ?>"><span class="icon icon-flatscreen"></span>Painel</a></li>
            <li{eventos}><a href="<?php echo base_url(); ?>eventos"><span class="icon icon-events"></span>Eventos</a></li>
            <li{inscritos}><a href="<?php echo base_url(); ?>inscritos"><span class="icon icon-users"></span>Inscritos</a></li>
            <li{funcionarios}><a href="<?php echo base_url(); ?>funcionarios"><span class="icon icon-config"></span>Funcionarios</a></li>
            <li{limpar}><a href="<?php echo base_url(); ?>funcionarios/limpar"><span class="icon icon-pencil"></span>Limpar Base</a></li>
		<?php } else { ?>
            <li{inscritos}><a href="<?php echo base_url(); ?>inscritos"><span class="icon icon-users"></span>Inscritos</a></li>
		<?php } ?>
        </ul>{/menu_current}
        
        <div class="headerwidget">
        	<div class="earnings">
            	<div class="one_half" style="width: 100%">
                	<h4>Evento atual</h4>
                    <h2>{evento_atual}</h2>
                </div><!--one_half-->
            </div><!--earnings-->
        </div><!--headerwidget-->
    </div><!--header-->
    