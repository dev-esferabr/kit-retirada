	<div class="pageheader">
		<h1 class="pagetitle">Eventos</h1>
		<span class="pagedesc">Gerenciar conflito de numero de peito repetido.</span>
		
		<ul class="hornav" style="height: 1px;">&nbsp;</ul>
	</div><!--pageheader-->
    
    <div id="contentwrapper" class="contentwrapper">
        
        <div id="event_new" class="subcontent">
			<div style="text-align: center;">
				<form action="{base_url}eventos/resolver_conflito/{ID}" method="post" style="display: inline-block" id="form_conflito">
					<div class="contenttitle2" style="float: left;">
						<h3>Numeros de peitos já cadastrados no ativo.</h3>
					</div>
					
					<br clear="all" />
					
					<table cellpadding="0" cellspacing="0" border="0" class="stdtable" style="width: 700px;" align="center">
						<thead>
							<tr>
								<th class="head0" width="*">Usuario</th>
								<th class="head1" width="130">Numero de peito atual</th>
								<th class="head0">Novo numero de peito</th>
							</tr>
						</thead>
						<tbody>{ATLETAS}
							<tr>
								<td style="width: 380px; text-align: left;">
									<a href="#" class="retirado_sim" data-id="{cod_inscritos_novo}">{nome_completo}</a>
								</td>
								<td style="text-align: right; width: 130px;">
									{nm_peito}
								</td>
								<td style="width: 130px;">
									<input name="nm_peito[{cod_inscritos_novo}]" style="width: 100%; text-align: right;" class="nm_peito_valido">
								</td>
							</tr>{/ATLETAS}
						</tbody>
					</table>
					
					<br clear="all" />
					
					<input type="submit" class="submit radius2" value="Salvar" style="float: right;">
				</form>
			</div>
        </div><!--contentwrapper-->
		
        <div id="event_old" class="subcontent">
		</div>
        
        <br clear="all" />
    </div>
<script>
	jQuery(document).ready(function(e) {
		checkNmPeitoCadastrado({ID});
                checkPedidoCadastrado({ID});
		triggerRetiradoSim();
		
		jQuery('#form_conflito').submit(function() {
			var tudoPreenchido = true;
			jQuery(this).find('input').each(function(index, element) {
				if(jQuery(this).val() == '')
					tudoPreenchido = false;
			});
			
			if(!tudoPreenchido) alert('Preencha todos os numeros de peito em conflito.');
			return tudoPreenchido;
		});
	});
</script>