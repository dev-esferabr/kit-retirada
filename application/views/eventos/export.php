<meta http-equiv="Content-type" content="text/html; charset=utf-8" />
<table border='0'>
  <tr>
    <td>Evento</td>
    <td>Nome</td>
    <td>ID Pedido</td>
    <td>Documento</td>
    <td>Data Pedido</td>
    <td>Modalidade</td>
    <td>Categoria</td>
    <td>Camiseta</td>
    <td>Número de Peito</td>
    <td>Data Retirado</td>
    <td>Retirado</td>
  </tr>
  <?php foreach($dados as $value):?>
  <tr>
    <td><?=$value->evento?></td>
    <td><?=$value->nome?></td>
    <td><?=$value->id_pedido?></td>
    <td><?=$value->documento?></td>
    <td><?=$value->dt_pedido?></td>
    <td><?=$value->modalidade?></td>
    <td><?=$value->categoria?></td>
    <td><?=$value->camiseta?></td>
    <td><?=$value->nm_peito?></td>
    <td><?=$value->retirado_data?></td>
    <td><?=$value->retirado?></td>
  </tr>
  <?php endforeach;?>
</table>
<?php
// Configurações header para forçar o download
header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
header ("Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT");
header ("Cache-Control: no-cache, must-revalidate");
header ("Pragma: no-cache");
header ("Content-Type: application/x-msexcel");
header ('Content-Disposition: attachment; filename="RetiradaKit.xls"');
header ("Content-Description: PHP Generated Data" );
exit();
?>