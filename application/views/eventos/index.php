	<div class="pageheader">
		<h1 class="pagetitle">Eventos</h1>
		<span class="pagedesc">Gerencie os eventos utilizado na Retirada de Kits.</span>
		
		<ul class="hornav">
			<li class="current"><a href="#event_new">Futuros</a></li>
			<li><a href="#event_old">Já realizados</a></li>
		</ul>
	</div><!--pageheader-->
    
    <div id="contentwrapper" class="contentwrapper">
        
        <div id="event_new" class="subcontent">
			<div class="contenttitle2 nomargintop">
				<h3>Evento atual</h3>
			</div>
			
			<form action="{base_url}eventos/atual">
				<div class="overviewhead">
					<strong>Selecionar</strong>: &nbsp;&nbsp;
					<div class="overviewselect" style="float: none; display: inline-block;">
						<select id="overviewselect" name="id_evento">
							<optgroup label="Evento Atual">EVENTOS_ATUAL
								{EVENTOS_ATUAL}<option value="{cod_evento}" selected>{dtevento} - {nome}</option>{/EVENTOS_ATUAL}
							</optgroup>
							<optgroup label="Eventos Baixados">
								<?php foreach($EVENTOS_FUTUROS as $name => $value) { ?>
									<option value="<?php echo $value->cod_evento; ?>"><?php echo end(explode('</span>', $value->dtevento)); ?> - <?php echo $value->nome; ?></option>
								<?php } ?>
							</optgroup>
						</select>
					</div><!--floatright-->
					<input type="submit" value="Alterar" style="float: right;">
				</div><!--overviewhead-->
			</form>
			
			<div class="contenttitle2">
				<h3>Baixar novo evento</h3>
			</div>
			
			<form action="{base_url}eventos/carregar" onsubmit="showLoading(); return true;">
				<div class="overviewhead">
					<?php if($this->integracao->temInternet) { ?>
						<strong>Selecionar</strong>: &nbsp;&nbsp;
						<div class="overviewselect" style="float: none; display: inline-block;">
							<select id="overviewselect" name="id_evento">
								<option value="">Selecione um evento</option>
								{BAIXAR}<option value="{id_evento}">{dt_evento} - {ds_evento}</option>{/BAIXAR}
							</select>
						</div><!--floatright-->
						<input type="submit" value="Baixar evento" style="float: right;">
					<?php } else { ?>
						<strong>Sem conexão com a internet.</strong>
					<?php } ?>
				</div><!--overviewhead-->
			</form>
			
			<div class="contenttitle2">
				<h3>Eventos</h3>
			</div>
			<table cellpadding="0" cellspacing="0" border="0" class="stdtable" id="lista_eventos">
				<thead>
					<tr>
						<th class="head0">Data</th>
						<th class="head1">Evento</th>
						<th class="head0">Quantidade<br />Inscritos</th>
						<th class="head1">Quantidade<br />Kits Retirado</th>
						<th class="head0">% Retirados</th>
						<th class="head1">Pedidos<br />realizados</th>
						<th class="head0">Sincronizar com<br /><div class="center" style="width: 100px;">Ativo.com</div></th>
						<th class="head1">Sincronizado</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th class="head0">Data</th>
						<th class="head1">Evento</th>
						<th class="head0">Quantidade<br />Inscritos</th>
						<th class="head1">Quantidade<br />Kits Retirado</th>
						<th class="head0">% Retirados</th>
						<th class="head1">Pedidos<br />realizados</th>
						<th class="head0">Sincronizar com<br /><div class="center" style="width: 100px;">Ativo.com</div></th>
						<th class="head1">Sincronizado</th>
					</tr>
				</tfoot>
				<tbody>{EVENTOS_FUTUROS}
					<tr>
						<td>{dtevento}</td>
						<td><a href="<?php echo base_url(); ?>eventos/gerenciar/{cod_evento}">{nome}</a></td>
						<td>{total}</td>
						<td>{retirados}</td>
						<td class="center">{porcentagem}%</td>
						<td>{pedidos_novos}</td>
						<td class="sincronizar">
							<?php if($this->integracao->temInternet) { ?>
								<a href="<?php echo base_url(); ?>eventos/enviar?id_evento={cod_evento}" onclick="showLoading(); return true;">Enviar</a>
								-
								<a href="<?php echo base_url(); ?>eventos/carregar?id_evento={cod_evento}" onclick="showLoading(); return true;">Receber</a>
							<?php } else { ?>
								<strong>Sem conexão com a internet.</strong>
							<?php } ?>
                                                                <br>
                                                                <a href="<?php echo base_url(); ?>eventos/export?id_evento={cod_evento}">Export</a>
						</td>
						{sincronizado}
					</tr>{/EVENTOS_FUTUROS}
				</tbody>
			</table>
        </div><!--contentwrapper-->
		
        <div id="event_old" class="subcontent">
			<div class="contenttitle2">
				<h3>Eventos</h3>
			</div>
			<table cellpadding="0" cellspacing="0" border="0" class="stdtable" id="lista_eventos2">
				<thead>
					<tr>
						<th class="head0">Data</th>
						<th class="head1">Evento</th>
						<th class="head0">Quantidade<br />Inscritos</th>
						<th class="head1">Quantidade<br />Kits Retirado</th>
						<th class="head0">% Retirados</th>
						<th class="head1">Pedidos<br />realizados</th>
						<th class="head0">Sincronizar com<br /><div class="center" style="width: 100px;">Ativo.com</div></th>
						<th class="head1">Sincronizado</th>
					</tr>
				</thead>
				<tfoot>
					<tr>
						<th class="head0">Data</th>
						<th class="head1">Evento</th>
						<th class="head0">Quantidade<br />Inscritos</th>
						<th class="head1">Quantidade<br />Kits Retirado</th>
						<th class="head0">% Retirados</th>
						<th class="head1">Pedidos<br />realizados</th>
						<th class="head0">Sincronizar com<br /><div class="center" style="width: 100px;">Ativo.com</div></th>
						<th class="head1">Sincronizado</th>
					</tr>
				</tfoot>
				<tbody>
                                    {EVENTOS_PASSADO}
					<tr>
						<td>{dtevento}</td>
						<td><a href="<?php echo base_url(); ?>eventos/gerenciar/{cod_evento}">{nome}</a></td>
						<td>{total}</td>
						<td>{retirados}</td>
						<td class="center">{porcentagem}%</td>
						<td>{pedidos_novos}</td>
						<td class="sincronizar">
							<?php if($this->integracao->temInternet) { ?>
								<a href="<?php echo base_url(); ?>eventos/enviar?id_evento={cod_evento}" onclick="showLoading(); return true;">Enviar</a>
								-
								<a href="<?php echo base_url(); ?>eventos/carregar?id_evento={cod_evento}" onclick="showLoading(); return true;">Receber</a>
							<?php } else { ?>
								<strong>Sem conexão com a internet.</strong>
							<?php } ?>
						</td>
						{sincronizado}
					</tr>{/EVENTOS_PASSADO}
				</tbody>
			</table>
		</div>
        
        <br clear="all" />
        
	</div><!-- centercontent -->
<script> jQuery('#overviewselect, input:checkbox').uniform(); </script>
<script>
	jQuery(document).ready(function(e) {
		jQuery('#lista_eventos').dataTable({"sPaginationType": "full_numbers", "aaSorting" : [[0, 'asc']]});
		jQuery('#lista_eventos2').dataTable({"sPaginationType": "full_numbers", "aaSorting" : [[0, 'desc']]});
	});
	
	function showLoading() {
		// Acertar Load
		var tamanhoPagina = jQuery(document).height();
		jQuery('body').append('<div id="loadPage"><span>&nbsp;</span></div>');
		jQuery('#loadPage').height(tamanhoPagina);
	}
</script>
<style>
	.stdtable .center {
		text-align: center;
	}
	
	.stdtable .falta_sincronizar {
		border: 1px solid #e18b7c !important;
		background: #fad5cf;
		text-align: center;
		font-weight: bold;
	}
	
	.stdtable .sincronizado {
		border: 1px solid #5DCF6B !important;
		background: #BBFEB9;
		text-align: center;
		font-weight: bold;
	}
	
	.stdtable .sincronizar {
		text-align: center;
	}
	
	.stdtable .sincronizar a {
		font-weight: bold;
		color: #039;
	}
</style>