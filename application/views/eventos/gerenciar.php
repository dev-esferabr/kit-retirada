
	<div class="pageheader">
		<h1 class="pagetitle">Eventos</h1>
		<span class="pagedesc">Gerencie os eventos utilizado na Retirada de Kits.</span>
		
		<ul class="hornav" style="height: 1px;">&nbsp;</ul>
	</div><!--pageheader-->
    
    <div id="contentwrapper" class="contentwrapper">
        
        <div id="event_new" class="subcontent">
			<div class="contenttitle2">
				<h3>Cadastro de Camisetas</h3>
			</div>
			<form action="{base_url}estoque/adicionar/{ID}" method="post">
				<table cellpadding="0" cellspacing="0" border="0" class="stdtable">
					<thead>
						<tr>
							<th class="head0">Tamanho</th>
							<th class="head1">Quantidade</th>
							<th class="head0">Ação</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td width="30%">
								<div class="overviewselect" style="float: left;">
									<select id="overviewselect" name="cod_tamanho_camiseta" required="required">
										<option value="">Selecione</option>
										{TAMANHOS}<option value="{cod_tamanho_camiseta}">{nome}</option>{/TAMANHOS}
									</select>
								</div><!--floatright-->
								<script> jQuery('#overviewselect, input:checkbox').uniform(); </script>
							</td>
							<td style="text-align: left;">
								<input type="text" name="total" class="smallinput">
							</td>
							<td style="text-align: right;">
								<input type="submit" value="Inserir" style="float: right;">
							</td>
						</tr>
						<tr>{QUANTIDADE_LEVADA}
							<td>{nome}</td>
							<td style="text-align: right;">{levados}</td>
							<td style="text-align: right;">
								<a href="<?php echo $base_url; ?>estoque/remover/<?php echo $ID; ?>/{cod_tamanho_camiseta}" class="btn btn_red btn_trash"><span style="color: #FFF;">Excluir</span></a>
							</td>
						</tr>{/QUANTIDADE_LEVADA}
					</tbody>
				</table>
			</form>
			
			<br clear="all" />
			
			<div class="contenttitle2">
				<h3>Controle de estoque</h3>
			</div>
			<table width="100%" style="margin-top: -20px;">
				<tr>
					<td style="padding-right: 40px;" width="25%">
						<table cellpadding="0" cellspacing="0" class="stdtable table invoicefor">
							<thead>
								<th class="head1" colspan="2">Quantidade levada</th>
							</thead>
							<tbody>{QUANTIDADE_LEVADA}
								<tr>
									<td width="50%"><strong>{nome}</strong></td>
									<td width="50%">{levados}</td>
								</tr>{/QUANTIDADE_LEVADA}
							</tbody>
						</table>
					</td>
					<td style="padding-right: 40px;" width="25%">
						<table cellpadding="0" cellspacing="0" class="stdtable table invoicefor">
							<thead>
								<th class="head1" colspan="2">Quantidade vendida</th>
							</thead>
							<tbody>{QUANTIDADE_VENDIDA}
								<tr>
									<td width="50%"><strong>{nome}</strong></td>
									<td width="50%">{total}</td>
								</tr>{/QUANTIDADE_VENDIDA}
							</tbody>
						</table>
					</td>
					<td style="padding-right: 40px;" width="25%">
						<table cellpadding="0" cellspacing="0" class="stdtable table invoicefor">
							<thead>
								<th class="head1" colspan="2">Quantidade retirada</th>
							</thead>
							<tbody>{QUANTIDADE_RETIRADA}
								<tr>
									<td width="50%"><strong>{nome}</strong></td>
									<td width="50%">{total}</td>
								</tr>{/QUANTIDADE_RETIRADA}
							</tbody>
						</table>
					</td>
					<td width="25%">
						<table cellpadding="0" cellspacing="0" class="stdtable table invoicefor">
							<thead>
								<th class="head1" colspan="2">Quantidade de sobra</th>
							</thead>
							<tbody>{QUANTIDADE_SOBRA}
								<tr>
									<td width="50%"><strong>{nome}</strong></td>
									<td width="50%">{total}</td>
								</tr>{/QUANTIDADE_SOBRA}
							</tbody>
						</table>
					</td>
				</tr>
			</table>
			
			<br clear="all" />
			
			<div class="contenttitle2">
				<h3>Controle de estoque</h3>
			</div>
			<table cellpadding="0" cellspacing="0" border="0" class="stdtable">
				<thead>
					<tr>
						<th class="head0">Produto</th>
						<th class="head1">Caracteristica 1</th>
						<th class="head0">Caracteristica 2</th>
						<th class="head1">Quantidade</th>
						<th class="head1">Retirados</th>
					</tr>
				</thead>
				<tbody>{PRODUTOS}
					<tr id="id_2">
						<td>{ds_titulo}</td>
						<td>{ds_recurso}</td>
						<td>{ds_recurso2}</td>
						<td>{total}</td>
						<td>{retirado}</td>
					</tr>{/PRODUTOS}
				</tbody>
			</table>
        </div><!--contentwrapper-->
		
        <div id="event_old" class="subcontent">
		</div>
        
        <br clear="all" />
    </div>