<?php

if ( ! defined('BASEPATH') )
    exit( 'No direct script access allowed' );

class Nativesession {
    public function __construct() {
		session_start();
	}
	
	public function userdata($key = 'userdata') {
		return $this->get( $key );
	}
	
	public function set_userdata($key = 'userdata', $value = '') {
		if(is_array($key)) {
			foreach($key as $name => $value)
				$this->set( $name, $value );
			return true;
		} else
			return $this->set( $key, $value );
	}
	
	public function unset_userdata($key = 'userdata') {
		return $this->delete( $key );
	}

    public function set( $key, $value )
    {
        $_SESSION[$key] = $value;
    }

    public function get( $key )
    {
        return isset( $_SESSION[$key] ) ? $_SESSION[$key] : null;
    }

    public function regenerateId( $delOld = false )
    {
        session_regenerate_id( $delOld );
    }

    public function delete( $key )
    {
        unset( $_SESSION[$key] );
    }
}