<?php

if ( ! defined('BASEPATH') )
    exit( 'No direct script access allowed' );

class Integracao {
	//public $esferabr_ws_url = 'http://localhost/webservice/public/Ativo';
	public $esferabr_ws_url = 'http://webservices.esferabr.com.br/Ativo';
	public $temInternet = false;
	
	public function __construct() {
            
		$CI =   &get_instance();
		$this->db = $CI->db;
		
		// Verificar se a internet está ligada
		$ligada = $this->db->select('internet_ligada')->from('internet_ligada')->limit(1)->get()->row()->internet_ligada;
		$this->temInternet = ($ligada == 'S');
	}
	
	// Ligar / Desliga Internet
	public function onOff($ligada) {
		$hrUpdate = date('Y-m-d H:i:s', strtotime('+30 seconds'));
		$this->db->update('internet_ligada', array(	'internet_ligada' => $ligada,
													'dt' => $hrUpdate), array('dt <= ' => date('Y-m-d H:i:s')));
		if($ligada != 'S')
			$this->temInternet = false;
	}
	
	// Pegar Próximos eventos do WebService
	public function listNextEvents() {
		// Verificar se tem acesso a internet
		if(!$this->temInternet)
			return array();
		
		return $this->ws_request('/Evento', array('modo' => 'futuros', 'fl_casa' => 2, 'count' => 9999));
	}
	
	// Carregar Evento
	public function loadEvents($id,$retirada_model) {
		// Carregar dados do ativo
		$dados = $this->ws_request('/EventoById', array('id' => $id));
		if($this->temInternet === false) // Verificar se deu pau na internet
			return 'Falha de conexão 0';
		
		// Array do Evento
		$evento = array();
		$evento['cod_evento'] = $dados[0]->id_evento;
		$evento['nome'] = $dados[0]->ds_evento;
		$evento['dtevento'] = substr($dados[0]->dt_evento, 0, 10) . ' ' . $dados[0]->hr_evento;
		$evento['fl_encerrar_inscricao'] = $dados[0]->fl_encerrar_inscricao;
		$evento['dt_alterado'] = date('Y-m-d H:i:s');
		
		// Modalidades do evento
		$modalidade = array();
		foreach($dados[0]->percursos as $name => $value) {
			$aux = array();
			$aux['cod_modalidade'] = $value->id_modalidade;
			$aux['nome'] = $value->nm_modalidade;
                        $aux['nr_peito_extra_de'] = $value->nr_peito_extra_de;
                        $aux['nr_peito_extra_ate'] = $value->nr_peito_extra_ate;
                        $aux['nome'] = $value->nm_modalidade;
			$aux['dt_alterado'] = date('Y-m-d H:i:s');
			
			$modalidade[] = $aux;
		}
		
		// Categoria do evento
		$categoria = array();
		foreach($dados[0]->kits_full as $name => $value) {
			$aux = array();
			$aux['cod_categoria'] = $value->id_categoria;
			$aux['nome'] = $value->nome;
			$aux['descricao'] = $value->ds_kit;
			$aux['dt_alterado'] = date('Y-m-d H:i:s');
			
			$categoria[] = $aux;
		}
//print_r($dados[0]->kits_full);exit();
		
		// Pegar Pedidos do Evento
		$dados = $this->ws_request('/Inscritos', array('id_evento' => $id, 'id_status' => '2'));
		if($this->temInternet === false) // Verificar se deu pau na internet
			return 'Falha de conexão 2';
		
		$inscritos = array();
		foreach($dados as $name => $value) {
			$aux = array();
			$aux['cod_inscritos'] = $value->id_pedido_evento;
			$aux['id_pedido'] = $value->id_pedido;
			$aux['id_evento'] = $value->id_evento;
			$aux['id_modalidade'] = $value->id_modalidade;
			$aux['id_categoria'] = $value->id_categoria;
			$aux['id_usuario'] = $value->id_usuario;
			$aux['id_tamanho_camiseta'] = $value->id_tamanho_camiseta;
			$aux['dt_cadastro'] = $value->dt_cadastro;
			$aux['dt_pedido'] = $value->dt_pedido;
			$aux['id_comprador'] = $value->id_comprador;
			$aux['nm_peito'] = $value->nr_peito;
			$aux['id_pedido_status'] = $value->id_pedido_status;
			$aux['form_pagamento'] = $value->form_pagamento;
			$aux['dt_pagamento'] = $value->dt_pagamento;
                        $aux['fl_capitao'] = $value->fl_capitao_revezamento;
			$aux['dt_alterado'] = date('Y-m-d H:i:s');
                        $aux['fl_balcao'] = ($value->fl_local_inscricao==2?1:0);
			
			$inscritos[] = $aux;
		}
		
		// Pegar Usuarios do Evento
		$dados = $this->ws_request('/UsuariosByEvento', array('id_evento' => $id, 'id_status' => '2'));
		if($this->temInternet === false) // Verificar se deu pau na internet
			return 'Falha de conexão 3';
		
		$usuarios = array();
		foreach($dados as $name => $value) {
			$aux = array();
			$aux['cod_usuario'] = $value->id_usuario;
			$aux['nome'] = $value->ds_nomecompleto;
			$aux['documento'] = $value->nr_documento;
			$aux['dtnascimento'] = $value->dt_nascimento;
			$aux['equipe'] = $value->ds_equipe;
			$aux['email'] = $value->ds_email;
			$aux['telefone'] = $value->nr_telefone;
			$aux['celular'] = $value->nr_celular;
			$aux['necessidades'] = ($value->nm_necessidades_especiais?$value->nm_necessidades_especiais:'N');
			$aux['dt_alterado'] = date('Y-m-d H:i:s');
                        $aux['fl_balcao'] = ($value->fl_local_inscricao==2?1:0);
			
			$usuarios[] = $aux;
		}
                
		// Pegar Usuarios do Evento
		$dados = $this->ws_request('/PedidosProduto', array('id_evento' => $id, 'id_status' => '2'));
		if($this->temInternet === false) // Verificar se deu pau na internet
			return 'Falha de conexão 4';
		
		$produtos = array();
		foreach($dados as $name => $value) {
			$aux = array();
			$aux['cod_pedido_produto'] = $value->id_pedido_produto;
			$aux['id_pedido'] = $value->id_pedido;
			$aux['id_evento'] = $id;
			$aux['ds_titulo'] = $value->ds_titulo;
			$aux['ds_recurso'] = $value->ds_recurso;
			$aux['ds_recurso2'] = $value->ds_recurso2;
			$aux['dt_alterado'] = date('Y-m-d H:i:s');
			
			$produtos[] = $aux;
		}
                
                // Pegar Usuarios do Evento
		$dados = $this->ws_request('/Camisetas', array('id_evento' => $id));
		if($this->temInternet === false) // Verificar se deu pau na internet
			return 'Falha de conexão 5';
                
                $camisetas = array();
		foreach($dados as $name => $value) {
			$aux = array();
			$aux['cod_tamanho_camiseta'] = $value->id_tamanho_camiseta;
			$aux['nome'] = $value->ds_tamanho;
			
			$camisetas[] = $aux;
		}
                
                $this->uploadExistentes($inscritos, $id, $retirada_model);
		
		// Salvar dados
		$this->save('evento', $evento, array('cod_evento' => $evento['cod_evento']));
		foreach($modalidade as $name => $value)
			$this->save('evento_modalidade', $value, array('cod_modalidade' => $value['cod_modalidade']));
		foreach($categoria as $name => $value)
			$this->save('evento_categoria', $value, array('cod_categoria' => $value['cod_categoria']));
		foreach($inscritos as $name => $value)
			$this->save('inscritos', $value, array('cod_inscritos' => $value['cod_inscritos']));
		foreach($usuarios as $name => $value)
			$this->save('usuario', $value, array('cod_usuario' => $value['cod_usuario']));
		foreach($produtos as $name => $value)
			$this->save('inscritos_produto', $value, array('cod_pedido_produto' => $value['cod_pedido_produto']));
                foreach($camisetas as $name => $value)
			$this->save('tamanho_camiseta', $value, array('cod_tamanho_camiseta' => $value['cod_tamanho_camiseta']));
		
		return true;
	}
	
        public function uploadExistentes($inscritos, $evento, $retirada_model){
            if(count($inscritos) > 0){
                $existentes = $retirada_model->getListExistenteUpload($evento);
                if(count($existentes) > 0){
                    foreach($existentes as $exis){
                        foreach($inscritos as $ins){
                            if($ins['id_pedido'] == $exis->id_pedido){
                                // Salvar dados da retirada
                                $data = array();
                                $data['eh_comprador'] = 1;
                                $data['nome'] = $exis->nome_completo;
                                $data['telefone'] = '';
                                $data['obs'] = 'Aprovado na sincronização';
                                $data['cod_funcionario'] = 1;
                                $data['id_pedido'] = $exis->id_pedido;
                                $cod_retirado_info = $retirada_model->saveInfo($data);
                                
                                $salvar[] = array($ins['cod_inscritos'], 0, 1);

                                // Salvar Retirada
                                foreach($salvar as $name => $value) {
                                        $data = array();
                                        $data['cod_retirado_info'] = $cod_retirado_info;
                                        $data['id_inscritos'] = $value[0];
                                        $data['id_inscritos_produto'] = $value[1];
                                        $data['id_evento'] = $evento;
                                        $data['retirado'] = $value[2];
                                        $retirada_model->save($data);
                                }
                                $retirada_model->saveListExistenteUploadPedido($ins['id_pedido']);
                            }
                        }
                    }
                }
            }
        }
        
	// Subir dados para o servidor
	public function uploadEvent($funcionarios_model, $retirada_model, $id) {		
		// Pegar dados de funcionarios para subir
            for($i=0;$i<=1000000;$i++){
                    $nr_peito_conflito = false;
                
                    $data = array();
                    $data['funcionarios'] = $funcionarios_model->getListUpload();
                    $data['info'] = $retirada_model->getListInfoUpload($id);
                    $data['nova'] = $retirada_model->getListNovaUpload($id);
                    $data['retirada'] = $retirada_model->getListUpload($id);
                    $data['alterados'] = $retirada_model->getListAlteradosUpload($id);
                    $data['usuariosalterados'] = $retirada_model->getListUsuariosAlteradosUpload();
                    
                    // Carregar dados do ativo
                    if(count($data['funcionarios']) > 0 || count($data['info']) > 0 || count($data['nova']) > 0 || count($data['retirada']) > 0 || count($data['alterados']) > 0 || count($data['usuariosalterados']) > 0){
                        $dados = $this->ws_request('/UploadRetirada', array('id_evento' => $id, 'dados' => $data));
                    }else{
                        break;
                    }
		
		// Verificar se deu pau na internet
		if($this->temInternet === false)
			return 'Falha de conexão - Sem Internet';
		
		 // Salvar os dados que subiram
		if($dados->save == 'OK') {
			// Pegar ID
			$i = 0;
			$ID = array();
			foreach($data['funcionarios'] as $name => $value) {
				if(count($ID[$i]) > 200) $i++;
				$ID[$i][] = $value->cod_funcionario;
			}
			$data['funcionarios'] = $ID;
			
			// Pegar ID
			$i = 0;
			$ID = array();
			foreach($data['info'] as $name => $value) {
				if(count($ID[$i]) > 200) $i++;
				$ID[$i][] = $value->cod_retirado_info;
			}
			$data['info'] = $ID;
			
			// Verificar se ouve conflito nos numeros de peito
			if($dados->nr_peito_conflito != 'nao') {
				$nr_peito_conflito = true;
				
				// Salvar numero de peito como conflito
				foreach($dados->nr_peito_conflito as $name => $value) {
					foreach($data['nova'] as $name2 => $value2) {
						// Não salvar inscrição como subida
						if($value2->cod_inscritos_novo == $value) {
							unset($data['nova'][$name2]);
						}
						
						// Salvar no cadastro que está em conflito
						$retirada_model->setConflito($value);
					}
				}
			}
			
			// Pegar ID
			$i = 0;
			$ID = array();
			foreach($data['nova'] as $name => $value) {
				if(count($ID[$i]) > 200) $i++;
				$ID[$i][] = $value->cod_inscritos_novo;
			}
			$data['nova'] = $ID;
			
			// Pegar ID
			$i = 0;
			$ID = array();
			foreach($data['retirada'] as $name => $value) {
				if(count($ID[$i]) > 200) $i++;
				$ID[$i][] = $value->cod_retirado;
			}
			$data['retirada'] = $ID;
			
			// Salvar
			if(count($data['funcionarios']) > 0)
				foreach($data['funcionarios'] as $name => $value)
					$funcionarios_model->saveListUpload($value);
			
			// Salvar
			if(count($data['info']) > 0)
				foreach($data['info'] as $name => $value)
					$retirada_model->saveListInfoUpload($value);
			
			// Salvar
			if(count($data['nova']) > 0)
				foreach($data['nova'] as $name => $value)
					$retirada_model->saveListNovaUpload($value);
			
			// Salvar
			if(count($data['retirada']) > 0)
				foreach($data['retirada'] as $name => $value)
					$retirada_model->saveListUpload($value);
                        
                        //Upload
                        if(count($data['alterados']) > 0)
                            $retirada_model->saveListAlteradosUpload($id);
                        
                        if(count($data['usuariosalterados']) > 0)
                            $retirada_model->saveListUsuariosAlteradosUpload();
		} else
			return 'Falha de conexão 2';
            }
            // Verificar se ouve conflito de numero de peito
            if($nr_peito_conflito)
                    return 'CONFLITO';
            else
                    return true;
	}
	
	// Salvar Dados
	function save($table, $data, $WHERE) {
		$q = $this->db->select('*')->from($table)->where($WHERE)->limit(1)->get();
		
		// Verificar se esta sendo inserido algum dado nulo
		foreach($data as $name => $value) {
			if($value == '')
				unset($data[$name]);
		}
		
		if ( $q->num_rows() > 0 )
			$this->db->where($WHERE)->update($table, $data);
		else
			$this->db->insert($table, $data);
	}
	
	// Conectar ao WebService do Ativo
	private function ws_request($url, $params=array()) {
		ini_set('max_execution_time', 0);
		set_time_limit(0);
		
		// Conectar ao Server
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $this->esferabr_ws_url . $url);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 1); 
		curl_setopt($ch, CURLOPT_TIMEOUT, 300);
		curl_setopt($ch, CURLOPT_POST, count($params) > 0);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
		$response = curl_exec($ch);
		//print_r($response);exit();
		
		// Verificar se deu TimeOut
		if(substr($response, 0, 6) == '<br />'
		|| $response === false
		|| $response === NULL
		|| curl_errno($ch) == 28
		|| curl_getinfo($ch, CURLINFO_HTTP_CODE) < 200
		|| curl_getinfo($ch, CURLINFO_HTTP_CODE) >= 400) {
/*print_r($params);
print_r( $this->esferabr_ws_url . $url);
print_r($response);
exit();*/
			$this->temInternet = false;
			$response = '[]';


		}
		
		// Fechar conexão
		curl_close($ch);
		
		return json_decode($response);
	}
}
