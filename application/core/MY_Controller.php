<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

error_reporting(E_ALL & ~E_DEPRECATED & ~E_NOTICE & ~E_WARNING);
ini_set("display_errors", 0);

class MY_Controller extends CI_Controller {
	public $displayTemplate = true;
	public $displayHead = true;
	public $menuCurrent = array('home' => ' class="current"',
								'eventos' => '',
								'inscritos' => '',
								'funcionarios' => '',
                                                                'limpar' => '');
	public $alert = false;
	
	public function __construct() {
		error_reporting(E_ERROR | E_WARNING | E_PARSE);
		parent::__construct();
	}
	
	public function getEventoAtual() {
		$this->load->model('evento_model');
		
		// Selecionar Evento Atual
		$eventoAtual = $this->evento_model->getEventoAtual();
		
		// Verificar se tem algum evento selecionado
		if($eventoAtual == NULL) {
			$eventoAtual = new stdClass();
			$eventoAtual->nome = 'Nenhum evento selecionado';
			$eventoAtual->cod_evento = '0';
			$eventoAtual->dtevento = '';
		}
		$eventoAtual->id_evento = $eventoAtual->cod_evento;
		
		return $eventoAtual;
	}
	
	public function checkLogin($NIVEL = 0) {
		$this->load->model('login_model');
		
		if(!$this->login_model->checkLogin($NIVEL)) {
			redirect('/login/');
			exit();
		}
	}
	
	public function hasPermission($nivel) {
		return $this->nativesession->userdata('logged_in')->NIVEL >= $nivel;
	}
	
	public function load( $view, $data = array() ) {
		// Pegar dados basicos
		$data['login'] = array($this->nativesession->userdata('logged_in'));
		$data['menu_current'] = array($this->menuCurrent);
		$data['evento_atual'] = $this->getEventoAtual()->nome;
		$data['ADMIN'] = $this->hasPermission(1);
		$data['NIVEL_ADMIN'] = ($this->nativesession->userdata('logged_in')->administrador == 1);
		$data['displayHead'] = $this->displayHead;
		$data['base_url'] = base_url();
		
		// Verificar se tem mensagem de alerta
		$data['ALERT'] = (($this->alert)? '<script> alert("' . $this->alert . '"); </script>': '');
		
		// Display site
		if($this->displayTemplate) $this->parser->parse( 'template/head', $data );
		if($this->displayTemplate && $this->displayHead) $this->parser->parse( 'template/header', $data );
		$this->parser->parse( $view, $data );
		if($this->displayTemplate) $this->parser->parse( 'template/foot', $data );
	}
}
