<?php 

class Funcionarios_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
	
	public function get($id) {
        $this->db->select('*');
        $this->db->from('funcionario');
		$this->db->where('cod_funcionario', $id);
		
		return $this->db->get()->result();
	}
	
	public function getList() {
        $this->db->select(array('cod_funcionario', 'nome', 'login', 'administrador', 'ativo'));
        $this->db->from('funcionario');
        $this->db->where('ativo <> -1');
		$this->db->order_by('cod_funcionario', 'desc');
		return $this->db->get()->result();
	}
	
	public function insert($data) {
		$data['cod_funcionario'] = date('YmdHis') . rand(0, 3000);
		$data['dt_alterado'] = date('YmdHis');
		$this->db->insert('funcionario', $data);
	}
	
	public function update($data, $id) {
		$data['dt_alterado'] = date('YmdHis');
		$this->db->update('funcionario', $data, array('cod_funcionario' => $id));
	}
	
	public function remover($id) {
		$this->db->update('funcionario', array('ativo' => '-1', 'dt_alterado' => date('YmdHis')), array('cod_funcionario' => $id));
	}
	
	public function getListUpload() {
        $this->db->select(array('cod_funcionario', 'nome'));
        $this->db->from('funcionario');
        $this->db->where('subiu', '0');
		return $this->db->get()->result();
	}
	
	public function saveListUpload($id) {
		$this->db->where_in('cod_funcionario', $id);
		$this->db->update('funcionario', array('subiu' => '1'));
	}
        
        public function limpar() {
            $this->db->truncate('evento');
            $this->db->truncate('evento_categoria');
            $this->db->truncate('evento_modalidade');
            $this->db->truncate('usuario');
            $this->db->truncate('inscritos');
            $this->db->truncate('inscritos_novo');
            $this->db->truncate('retirado_info');
            $this->db->truncate('retirado');
            $this->db->truncate('inscritos_produto');
            $this->db->truncate('pedidos_existentes');
            $this->db->truncate('tamanho_camiseta');
	}
}