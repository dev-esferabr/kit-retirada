<?php 

class Retirada_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
	
	// Pegar dados da retirada pelo ID do item
    public function getRetiradaByIten($id_inscritos = 0, $id_inscritos_produto = 0) {
        $this->db->select('*');
        $this->db->from('retirado');
		$this->db->where('id_inscritos', $id_inscritos);
		$this->db->where('id_inscritos_produto', $id_inscritos_produto);
		$this->db->order_by('cod_retirado', 'DESC');
		$this->db->limit(1);
		
		return $this->db->get()->row();
    }
	
	// Salvar informações da Retirada
	public function saveInfo($data) {
		$data['cod_retirado_info'] = date('YmdHis') . $data['id_pedido'] . rand(0, 3000);
		$data['dt_alterado'] = date('YmdHis');
		$this->db->insert('retirado_info', $data);
		return $data['cod_retirado_info'];
	}
	
	// Salvar Retirada
	public function save($data) {
		$data['cod_retirado'] = date('YmdHis') . $data['id_inscritos'] . $data['id_inscritos_produto'] . rand(0, 3000);
		$data['dt_alterado'] = date('YmdHis');
		$this->db->insert('retirado', $data);
		
		// Marcar somente o ultimo como atual
		$idUltimo = $this->db->select('cod_retirado')
							 ->from('retirado')
							 ->where('id_inscritos', $data['id_inscritos'])
							 ->where('id_inscritos_produto', $data['id_inscritos_produto'])
							 ->order_by('cod_retirado', 'DESC')
							 ->limit(1)
							 ->get()
							 ->row()
							 ->cod_retirado;
		
		$this->db->query('UPDATE retirado
						  SET ultimo = IF(' . $idUltimo . ' = cod_retirado, 1, 0)
						  WHERE id_inscritos = "' . $data['id_inscritos'] . '"
						  	AND id_inscritos_produto = "' . $data['id_inscritos_produto'] . '"', array());
	}
	
	// Pegar ID do Pedido
	public function getHistoricoPedido($id_pedido, $limit = 0) {
		$this->db->select(array('i.dt_alterado',
								'f.nome as funcionario',
								'i.nome',
								'i.telefone',
								'i.obs',
								'IF(NOT ISNULL(cod_retirado), GROUP_CONCAT(IF(retirado=1, "Retirado", "<span style=\"color: red;\">Devolvido</span>"), IF(id_inscritos > 0," Kit "," Produto "), IF(r.id_inscritos > 0, (
									SELECT u.nome
									FROM inscritos sub1
										LEFT JOIN usuario u ON u.cod_usuario = sub1.id_usuario
									WHERE sub1.cod_inscritos = r.id_inscritos
									LIMIT 1
								), (
									SELECT sub2.ds_titulo
									FROM inscritos_produto sub2
									WHERE sub2.cod_pedido_produto = r.id_inscritos_produto
									LIMIT 1
								)) SEPARATOR "<br>"), "Nenhuma ação realizada") as acao'));
		$this->db->from('retirado_info i');
		$this->db->join('retirado r', 'i.cod_retirado_info = r.cod_retirado_info', 'left');
		$this->db->join('funcionario f', 'i.cod_funcionario = f.cod_funcionario', 'left');
		$this->db->where('i.id_pedido', $id_pedido);
		$this->db->group_by('i.cod_retirado_info');
		$this->db->order_by('i.dt_alterado', 'DESC');
		
		return $this->db->get()->result();
	}
	
	// Pegar a porcentagem de retiradas que subiram
	public function getPCSubiu($id_evento) {
		$this->db->select(array('COUNT(*) as total', 'SUM(subiu) as subiu'));
		$this->db->from('retirado');
		$this->db->where('id_evento', $id_evento);
		$dados = $this->db->get()->row();
		
		return array($dados->subiu, $dados->total);
	}
	
	// Pegar historico do produto (Ultimo)
	public function getUltimoHistoricoProduto($id_inscritos, $id_produto) {
		$this->db->select(array('i.*'));
		$this->db->from('retirado_unico u');
		$this->db->join('retirado r', 'r.cod_retirado = u.cod_retirado');
		$this->db->join('retirado_info i', 'i.cod_retirado_info = r.cod_retirado_info');
		$this->db->where('u.id_inscritos', $id_inscritos);
		$this->db->where('u.id_inscritos_produto', $id_produto);
		$this->db->limit(1);
		
		return $this->db->get()->row();
	}
	
	// Pegar lista de informações para subir ao servidor
	public function getListInfoUpload($id) {
		$this->db->select(array('i.*'));
		$this->db->from('retirado_info i');
		$this->db->join('inscritos p', 'p.id_pedido = i.id_pedido');
		$this->db->where('i.subiu', '0');
		$this->db->where('p.id_evento', $id);
                $this->db->limit(30);
		
		return $this->db->get()->result();
	}
	
	// Pegar lista de retiradas para subir ao servidor
	public function getListUpload($id) {
		$this->db->select(array('*'));
		$this->db->from('retirado');
		$this->db->where('subiu', '0');
		$this->db->where('id_evento', $id);
                $this->db->limit(30);
		
		return $this->db->get()->result();
	}
        
        // Pegar lista de retiradas para subir ao servidor
	public function getListAlteradosUpload($id) {
		$this->db->select(array('*'));
		$this->db->from('inscritos');
		$this->db->where('fl_alterado', '1');
		$this->db->where('id_evento', $id);
                $this->db->limit(30);
		
		return $this->db->get()->result();
	}
        
        // Alterar usuários
	public function getListUsuariosAlteradosUpload() {
		$this->db->select(array('*'));
		$this->db->from('usuario');
		$this->db->where('fl_alterado', '1');
		
		return $this->db->get()->result();
	}
	
	// Pegar lista de retiradas para subir ao servidor
	public function getListNovaUpload($id) {
		$this->db->select(array('*'));
		$this->db->from('inscritos_novo');
		$this->db->where('subiu', '0');
		$this->db->where('id_evento', $id);
		
		return $this->db->get()->result();
	}
        
        // Pegar lista de retiradas para subir ao servidor
	public function getListExistenteUpload($id) {
		$this->db->select(array('*'));
		$this->db->from('pedidos_existentes');
		$this->db->where('subiu', '0');
		$this->db->where('id_evento', $id);
		
		return $this->db->get()->result();
	}
	
	public function saveListInfoUpload($id) {
		$this->db->where_in('cod_retirado_info', $id);
		$this->db->update('retirado_info', array('subiu' => '1'));
	}
        
        public function saveListAlteradosUpload($id) {
		$this->db->update('inscritos', array('fl_alterado' => '0'), array('id_evento' => $id));
	}
        
        public function saveListUsuariosAlteradosUpload() {
		$this->db->update('usuario', array('fl_alterado' => '0'), array('fl_alterado' => 1));
	}
	
	public function saveListNovaUpload($id) {
		$this->db->where_in('cod_inscritos_novo', $id);
		$this->db->update('inscritos_novo', array('subiu' => '1'));
	}
        
        public function saveListExistenteUpload($id) {
		$this->db->where_in('id_pedido_existente', $id);
		$this->db->update('pedidos_existentes', array('subiu' => '1'));
	}
        
        public function saveListExistenteUploadPedido($id) {
		$this->db->where_in('id_pedido', $id);
		$this->db->update('pedidos_existentes', array('subiu' => '1'));
	}
	
	public function saveListUpload($id) {
		$this->db->where_in('cod_retirado', $id);
		$this->db->update('retirado', array('subiu' => '1'));
	}
	
	public function setConflito($cod_inscritos_novo) {
		$this->db->update('inscritos_novo', array('conflito_nm_peito' => true), array('cod_inscritos_novo' => $cod_inscritos_novo));
	}
        
}