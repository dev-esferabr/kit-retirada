<?php 

class Estoque_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
	
	public function getQuantidade($id_evento, $id_camiseta) {
		/////////////////////
	}
	
	public function getTamanhos($id) {
        $this->db->select('*');
        $this->db->from('tamanho_camiseta');
		$this->db->order_by('cod_tamanho_camiseta', 'asc');
		
		return $this->db->get()->result();
	}
	
	public function getQuantidadesLevadas($id) {
        $this->db->select(array('cod_tamanho_camiseta', 'levados', 'nome'));
        $this->db->from('estoque_camiseta');
        $this->db->join('tamanho_camiseta', 'id_tamanho_camiseta = cod_tamanho_camiseta');
		$this->db->where('id_evento', $id);
		$this->db->order_by('id_tamanho_camiseta', 'asc');
		
		return $this->db->get()->result();
	}
	
	public function insert($id_evento, $id_camiseta, $quantidade) {
		$this->db->delete('estoque_camiseta', array('id_evento' => $id_evento,
													'id_tamanho_camiseta' => $id_camiseta)); 
		$this->db->insert('estoque_camiseta', array('cod_estoque_camiseta' => $id_evento . '-' . $id_camiseta,
													'id_evento' => $id_evento,
													'id_tamanho_camiseta' => $id_camiseta,
													'levados' => $quantidade,
													'dt_alterado' => date('Y-m-d H:i:s')));
	}
	
	public function remover($id, $id2) {
		$dados = array('levados' => 0, 'dt_alterado' => date('Y-m-d H:i:s'));
		$this->db->where(array('id_evento' => $id , 'id_tamanho_camiseta' => $id2))->update('estoque_camiseta', $dados);
	}
}