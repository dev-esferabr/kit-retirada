<?php 

class Login_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
	
    public function login($username, $password, $tipo) {
		// Selecionar dados do usuario que está tentando logar
        $this->db->select('*');
        $this->db->from('funcionario');
		$this->db->where('login', $username);
		$this->db->where('senha', md5($password));
		$this->db->where('administrador >= ', $tipo);
		$this->db->where('ativo != ', '-1');
		$this->db->limit(1);
		
		return $this->db->get()->result();
    }
	
	public function saveLog($cod_funcionario) {
		$this->db->insert('funcionario_log', array(	'cod_funcionario_log' => date('YmdHis') . rand(0, 3000),
													'cod_funcionario' => $cod_funcionario));
	}
	
	public function ultimosLoginLog() {
        $this->db->select(array('funcionario.cod_funcionario',
								'funcionario.nome',
								'funcionario.avatar',
								'funcionario_log.dt_alterado'));
        $this->db->from('funcionario_log');
        $this->db->join('funcionario', 'funcionario.cod_funcionario = funcionario_log.cod_funcionario');
		$this->db->order_by('funcionario_log.dt_alterado', 'desc');
		$this->db->limit(7);
		
		return $this->db->get()->result();
	}
	
	public function checkLogin($nivel) {
		if($this->nativesession->userdata('logged_in')) {
			if(is_numeric($this->nativesession->userdata('logged_in')->cod_funcionario)
			&& $this->nativesession->userdata('logged_in')->cod_funcionario > 0
			&& $this->nativesession->userdata('logged_in')->NIVEL >= $nivel) {
				return true;
			} else
				return false;
		} else
			return false;
	}
}