<?php 

class Evento_model extends CI_Model {

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
	
	// Carregar lista de eventos
	public function getEventos($WHERE = array(), $ORDEM = array()) {
        $this->db->select('*');
        $this->db->from('evento');
		if(count($WHERE) > 0)
        	$this->db->where($WHERE);
		if(count($ORDEM) == 2)
        	$this->db->order_by($ORDEM[0], $ORDEM[1]);
		
		return $this->db->get()->result();
	}
	
	// Pegar lista dos ultimos 7 eventos carregado
	public function getEventosHome() {
        $this->db->select('*');
        $this->db->from('evento');
		$this->db->order_by('dtevento', 'desc');
		$this->db->limit(7);
		
		return $this->db->get()->result();
	}
	
	// Pegar ID dos Eventos já carregados
	public function getIDEventos() {
        $this->db->select('cod_evento');
        $this->db->from('evento');
		$IDS = $this->db->get()->result();
		
		$retorno = array();
		foreach($IDS as $name => $value) $retorno[] = $value->cod_evento;
		
		return $retorno;
	}
	
	// Setar Evento Atual
	public function setEventoAtual($id_evento) {
		$this->db->update('evento_atual', array('id_evento' => $id_evento, 'dt_alterado' => date('Y-m-d H:i:s')), $data, array('evento_atual' => '0'));
	}
	
	// Carregar lista de eventos
	public function getEventoAtual() {
		// Pegar Id Do Evento Atual
        $this->db->select('id_evento');
        $this->db->from('evento_atual');
		$this->db->limit(1);
		
		return $this->getEventoByID($this->db->get()->row()->id_evento);
	}
	
	// Carregar lista de eventos
	public function getEventoByID($id) {
		// Pegar Id Do Evento Atual
        $this->db->select('*');
        $this->db->from('evento');
		$this->db->where('cod_evento', $id);
		
		return $this->db->get()->row();
	}
}