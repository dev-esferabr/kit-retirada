<?php 

class Sync_model extends CI_Model {
	public $pastaBase = '/RetiradaKIT';
	private $serverToken = '5DWVYEKunQHzAvXg';
	private $SyncTable = array(	array('estoque_camiseta', 'cod_estoque_camiseta', true),
								array('evento', 'cod_evento', true),
								array('evento_atual', 'cod_atual', true),
								array('evento_categoria', 'cod_categoria', true),
								array('evento_modalidade', 'cod_modalidade', true),
								array('funcionario', 'cod_funcionario', true),
								array('funcionario_log', 'cod_funcionario_log', false),
								array('inscritos', 'cod_inscritos', true),
								array('tamanho_camiseta', 'cod_tamanho_camiseta', true),
								array('usuario', 'cod_usuario', true));

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
	
	// Retornar se é server
	public function isServer() {
        $this->db->select('server');
        $this->db->from('server');
		$this->db->where('cod_server', '0');
		
		return $this->db->get()->row()->server;
	}
	
	// Transformar esta estação em servidor
	public function thisServer() {
		$this->db->update('server', array('server' => 1), array('cod_server' => 0));
	}
	
	// Transformar esta estação em servidor
	public function thisNotServer() {
		$this->db->update('server', array('server' => 0), array('cod_server' => 0));
	}
	
	// Gerar Token
	public function generateToken($time) {
		return md5($this->serverToken . $time);
	}
	
	// Checar Token
	public function checkToken($time, $token) {
		return $token == md5($this->serverToken . $time);
	}
	
	// Fazer Curl em outro IP
	public function curl($request, $IP, $POST = array()) {
		$url = 'http://' . $IP . $this->pastaBase . '/Sync/' . $request;
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT_MS, 1000); 
		curl_setopt($ch, CURLOPT_TIMEOUT_MS, 0);
		
		if(count($POST) > 0) {
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $POST);
		}
		
		$response = curl_exec($ch);
		curl_close($ch);
		
		return $response;
	}
	
	// Selecionar dados
	public function getFullData() {
		$retorno = array();
		
		// Pegar todos os dados das tabelas
		foreach($this->SyncTable as $name => $value) {
			// Selecionar ID
			$select = array($value[1]);
			
			// Verificar se tem data de alteração
			//if($value[2])
				$select[] = 'dt_alterado';
			
			// Selecionar dados
			$retorno[$value[0]] = $this->db->select($select)->from($value[0])->get()->result_array();
		}
		
		return $retorno;
	}
	
	// Verificar se tem algum dado diferente
	public function checkFullData($data) {
		$retorno = array();
		
		$data['estoque_camiseta'][] = array('cod_estoque_camiseta' => -1, 'dt_alterado' => '2015-05-03 20:46:15');
		$data['estoque_camiseta'][] = array('cod_estoque_camiseta' => '20708-920708-9', 'dt_alterado' => '2015-05-03 20:46:16');
		
		// Pegar todos os dados das tabelas
		foreach($this->SyncTable as $name => $value) {
			$retorno[$value[0]] = array();
			
			// Varrer dados
			foreach($data[$value[0]] as $name2 => $value2) {
				$WHERE = array($value[1] => $value2[$value[1]]);
				
				// Verificar se tem data de alteração
				//if($value[2])
					$WHERE['dt_alterado >='] = $value2['dt_alterado'];
				
				// Verificar se já tem este dado
				if($this->db->select('1 as um')->from($value[0])->where($WHERE)->get()->row()->um != '1')
					$retorno[$value[0]][] = $value2[$value[1]];
			}
		}
		
		return $retorno;
	}
	
	// Selecionar dados
	public function exportDataByID($IDs) {
		$retorno = array();
		
		// Pegar todos os dados das tabelas
		foreach($this->SyncTable as $name => $value) {
			// Selecionar dados
			$retorno[$value[0]] = array();
			if(count($IDs[$value[0]]) > 0)
				$retorno[$value[0]] = $this->db->select('*')->from($value[0])->where_in($value[1], $IDs[$value[0]])->get()->result_array();
		}
		
		return $retorno;
	}
	
	// Selecionar dados
	public function exportDataByDate($date) {
		$retorno = array();
		
		// Pegar todos os dados das tabelas
		foreach($this->SyncTable as $name => $value) {
			// Selecionar dados
			$retorno[$value[0]] = $this->db->select('*')->from($value[0])->where('dt_alterado >', $date)->get()->result_array();
		}
		
		return $retorno;
	}
	
	// Selecionar dados
	public function exportSave($data) {
		// Pegar todos os dados das tabelas
		foreach($this->SyncTable as $name => $value) {
			// Selecionar dados
			if(count($data[$value[0]]) > 0) {
				foreach($data[$value[0]] as $name2 => $value2) {
					if ( $this->db->from($value[0])->where($value[1], $value2[$value[1]])->get()->num_rows() > 0 ) {
						$id = $value2[$value[1]];
						unset($value2[$value[1]]);
						$this->db->update($value[0], $value2, array($value[1] => $id));
					} else {
						$this->db->insert($value[0], $value2);
					}
				}
			}
		}
	}
}