<?php 

class Inscritos_model extends CI_Model {
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
	
	public function getByID($id) {
		return $this->db->from('inscritos')->where('cod_inscritos', $id)->get()->row();
	}
	
	public function getModalidadeByID($id) {
		return $this->db->from('evento_modalidade')->where('cod_modalidade', $id)->get()->row();
	}
	
	public function getCategoriaByID($id) {
		return $this->db->from('evento_categoria')->where('cod_categoria', $id)->get()->row();
	}
	
	public function getCamisetaByID($id) {
		return $this->db->from('tamanho_camiseta')->where('cod_tamanho_camiseta', $id)->get()->row();
	}
	
	public function countInscricoes($WHERE = array()) {
		// Contar Inscritos
        $this->db->select('count(*) as total');
        $this->db->from('inscritos');
		$this->db->where($WHERE);
		$total = $this->db->get()->row();
		
		// Contar Produtos
        $this->db->select('count(*) as total');
        $this->db->from('inscritos_produto');
		$this->db->where($WHERE);
		$total2 = $this->db->get()->row();
		
		return intval($total->total + $total2->total);
	}
	
	public function countRetirados($WHERE = array()) {
        $this->db->select('sum(retirado) as total');
        $this->db->from('retirado_unico');
		$this->db->where($WHERE);
		$total = $this->db->get()->row();
		
		return intval($total->total);
	}
	
	public function countQuantidadeCamisetaVendida($id) {
        $this->db->select(array('cod_tamanho_camiseta', 'nome', 'count(*) as total'));
        $this->db->from('inscritos');
        $this->db->join('tamanho_camiseta', 'id_tamanho_camiseta = cod_tamanho_camiseta');
		$this->db->where('id_evento', $id);
		$this->db->order_by('id_tamanho_camiseta', 'asc');
		$this->db->group_by('id_tamanho_camiseta');
		
		return $this->db->get()->result();
	}
	
	public function countQuantidadeCamisetaRetiradas($id) {
        $this->db->select(array('id_tamanho_camiseta', 'nome', 'sum(retirado) as total'));
        $this->db->from('inscritos');
        $this->db->join('tamanho_camiseta', 'id_tamanho_camiseta = cod_tamanho_camiseta');
		$this->db->join('retirado_unico', 'retirado_unico.id_inscritos = inscritos.cod_inscritos');
		$this->db->where('inscritos.id_evento', $id);
		$this->db->order_by('id_tamanho_camiseta', 'asc');
		$this->db->group_by('id_tamanho_camiseta');
		
		return $this->db->get()->result();
	}
	
	public function getList($id, $where = array(), $where2 = array()) {
            
            $this->db->select(array('pedidos_existentes.id_pedido_existente AS cod_inscritos',
								'pedidos_existentes.id_pedido',
								'pedidos_existentes.nm_peito',
								'"" AS dt_pedido',
								'"" AS id_pedido_status',
								'"" AS form_pagamento',
								'"" AS id_comprador',
                                                                '"" AS nome_comprador',
                                                                '"" AS telefone_comprador',
								'"" AS dt_pagamento',
								'pedidos_existentes.nome_completo AS nome',
								'pedidos_existentes.nr_documento AS documento',
								'"" as categoria',
								'"" as modalidade',
                                                                '"" as camiseta',
                                                                '"" as fl_capitao',
								'"" as produtos',
                                                                '"Sim" as retirado',
								'dt_alterado as retirado_data'));
        $this->db->from('pedidos_existentes');
		$this->db->where('pedidos_existentes.id_evento', $id);
		$this->db->where('subiu',0);
		$this->db->order_by('id_pedido', 'asc');
		$this->db->group_by('pedidos_existentes.id_pedido');
		$query = $this->db->get_compiled_select();
            
            $this->db->select(array('inscritos.cod_inscritos',
								'inscritos.id_pedido',
								'inscritos.nm_peito',
								'inscritos.dt_pedido',
								'inscritos.id_pedido_status',
								'inscritos.form_pagamento',
								'inscritos.id_comprador',
                                                                'comprador.nome AS nome_comprador',
                                                                'comprador.telefone AS telefone_comprador',
								'inscritos.dt_pagamento',
								'usuario.nome',
								'usuario.documento',
								'evento_categoria.nome as categoria',
								'evento_modalidade.nome as modalidade',
                                                                'tamanho_camiseta.nome as camiseta',
                                                                'IF(inscritos.fl_capitao >= 1, "Sim", "Não") as fl_capitao',
								'GROUP_CONCAT(ds_titulo SEPARATOR "<br>") as produtos',
								'IF((SELECT retirado
									 FROM retirado
									 WHERE cod_inscritos = id_inscritos
									 ORDER BY dt_alterado DESC
									 LIMIT 1)>0, "Sim", "Não") as retirado',
								'(SELECT dt_alterado
								  FROM retirado
								  WHERE cod_inscritos = id_inscritos
								  ORDER BY dt_alterado DESC
								  LIMIT 1) as retirado_data'));
        $this->db->from('inscritos');
		$this->db->join('usuario', 'cod_usuario = id_usuario', 'left');
                $this->db->join('usuario AS comprador', 'comprador.cod_usuario = id_comprador', 'left');
		$this->db->join('evento_categoria', 'cod_categoria = id_categoria', 'left');
		$this->db->join('evento_modalidade', 'cod_modalidade = id_modalidade', 'left');
		$this->db->join('inscritos_produto', 'inscritos_produto.id_pedido = inscritos.id_pedido', 'left');
                $this->db->join('tamanho_camiseta', 'tamanho_camiseta.cod_tamanho_camiseta = inscritos.id_tamanho_camiseta', 'left');
		$this->db->where('inscritos.id_evento', $id);
		if(count($where) > 0) $this->db->where($where);
		$this->db->order_by('id_pedido', 'asc');
		$this->db->group_by('inscritos.cod_inscritos');
		$query1 = $this->db->get_compiled_select();
		
        $this->db->select(array('cod_inscritos_novo as cod_inscritos',
								'cod_inscritos_novo as id_pedido',
								'nm_peito',
								'inscritos_novo.dt_alterado as dt_pedido',
								'2 as id_pedido_status',
								'IF(forma_pagamento, "Dinheiro", "Cartão") as form_pagamento',
								'0 id_comprador',
                                                                '"" AS nome_comprador',
                                                                '"" AS telefone_comprador',
								'inscritos_novo.dt_alterado as dt_pagamento',
								'nome_completo as nome',
								'v_nr_documento as documento',
								'evento_categoria.nome as categoria',
								'evento_modalidade.nome as modalidade',
                                                                'tamanho_camiseta.nome as camiseta',
                                                                '"" as fl_capitao',
								'"" as produtos',
								'"Sim" as retirado',
								'inscritos_novo.dt_alterado as retirado_data'));
        $this->db->from('inscritos_novo');
		$this->db->join('evento_categoria', 'cod_categoria = categoria', 'left');
		$this->db->join('evento_modalidade', 'cod_modalidade = modalidade', 'left');
                $this->db->join('tamanho_camiseta', 'tamanho_camiseta.cod_tamanho_camiseta = inscritos_novo.camiseta', 'left');
		$this->db->where('id_evento', $id);
		if(count($where2) > 0) $this->db->where($where2);
		$query2 = $this->db->get_compiled_select();
		
		//if(count($where) > 0) $this->db->where($where);
		
		
		return $this->db->query('(' . $query . ') UNION ALL (' . $query1 . ') UNION ALL (' . $query2 . ')')->result();
	}
	
	public function getListByPedido($id_evento, $id) {
        $this->db->select(array('inscritos.cod_inscritos',
								'inscritos.id_pedido',
								'inscritos.nm_peito',
								'inscritos.dt_pedido',
								'inscritos.id_pedido_status',
								'inscritos.form_pagamento',
								'inscritos.id_comprador',
								'inscritos.dt_pagamento',
                                                                'inscritos.fl_capitao',
								'usuario.cod_usuario',
								'usuario.nome',
								'usuario.documento',
								'evento_categoria.nome as categoria',
                                                                'evento_categoria.cod_categoria as id_categoria',
                                                                'evento_modalidade.cod_modalidade as id_modalidade',
								'evento_modalidade.nome as modalidade',
								'tamanho_camiseta.nome as camiseta',
                                                                'inscritos.fl_balcao',
								'IF((SELECT retirado
									 FROM retirado
									 WHERE cod_inscritos = id_inscritos
									 ORDER BY dt_alterado DESC
									 LIMIT 1)>0, "Sim", "Não") as retirado',
								'(SELECT dt_alterado
								  FROM retirado
								  WHERE cod_inscritos = id_inscritos
								  ORDER BY dt_alterado DESC
								  LIMIT 1) as retirado_data'));
        $this->db->from('inscritos');
		$this->db->join('usuario', 'cod_usuario = id_usuario', 'left');
		$this->db->join('evento_categoria', 'cod_categoria = id_categoria', 'left');
		$this->db->join('evento_modalidade', 'cod_modalidade = id_modalidade', 'left');
		$this->db->join('tamanho_camiseta', 'cod_tamanho_camiseta = id_tamanho_camiseta', 'left');
		$this->db->where('id_evento', $id_evento);
		$this->db->where('id_pedido', $id);
		$this->db->order_by('usuario.nome', 'asc');
		
		return $this->db->get()->result();
	}
	
	public function getCountProdutosEvento($id, $id_pedido = 0) {
        $this->db->select(array('cod_pedido_produto',
								'ds_titulo',
								'ds_recurso',
								'ds_recurso2',
								'count(*) as total',
								'SUM(retirado) as retirado'));
        $this->db->from('inscritos_produto p');
		$this->db->join('retirado_unico', 'retirado_unico.id_inscritos_produto = p.cod_pedido_produto', 'left');
		$this->db->where('p.id_evento', $id);
		if($id_pedido > 0) $this->db->where('p.id_pedido', $id_pedido);
		$this->db->order_by('ds_titulo', 'asc');
		$this->db->group_by(array('ds_titulo', 'ds_recurso', 'ds_recurso2'));
		
		return $this->db->get()->result();
	}
	
	public function getUsuarioByID($id,$fl_balcao=0) {
        $this->db->select('*');
        $this->db->from('usuario');
		$this->db->where('cod_usuario', $id)->where('fl_balcao',$fl_balcao);
		
		return $this->db->get()->row();
	}
	
	// Dados para novas inscrições
	public function getCidade($id) {
        $this->db->select('*');
        $this->db->from('sa_cidade');
		$this->db->where('id_estado', $id);
		
		return $this->db->get()->result();
	}
	public function getModalidade($id_evento) {
        $this->db->select(array('m.cod_modalidade', 'm.nome'));
        $this->db->from('inscritos i');
        $this->db->join('evento_modalidade m', 'i.id_modalidade = m.cod_modalidade');
		$this->db->where('i.id_evento', $id_evento);
		$this->db->group_by('m.cod_modalidade');
		
		return $this->db->get()->result();
	}
	public function getCategoria($id_evento) {
        $this->db->select(array('c.cod_categoria', 'c.nome', 'i.id_modalidade'));
        $this->db->from('inscritos i');
        $this->db->join('evento_categoria c', 'i.id_categoria = c.cod_categoria');
		$this->db->where('i.id_evento', $id_evento);
		$this->db->group_by('c.cod_categoria');
		
		return $this->db->get()->result();
	}
	
	// Salvar Nova Inscrição
	public function nova($data) {
		$data['cod_inscritos_novo'] = date('YmdHis') . $data['id_evento'] . rand(0, 3000);
		$data['dt_alterado'] = date('YmdHis');
		$this->db->insert('inscritos_novo', $data);
		
		return $data['cod_inscritos_novo'];
	}
        
        // Salvar Inscrição Existente
	public function existente($data) {
		$data['id_pedido_existente'] = date('YmdHis') . $data['id_evento'] . rand(0, 3000);
		$data['dt_alterado'] = date('YmdHis');
		$this->db->insert('pedidos_existentes', $data);
		
		return $data['cod_inscritos_novo'];
	}
	
	// Pegar pedido novo pelo ID
	public function getPedidoNovoByID($id_evento, $id_inscritos) {
		$this->db->select('*');
		$this->db->from('inscritos_novo');
		if($id_evento > 0) $this->db->where('id_evento', $id_evento);
		$this->db->where('cod_inscritos_novo', $id_inscritos);
		
		return $this->db->get()->row();
	}
	
	// Contar Nova Inscrição
	public function countPCNova($id_evento) {
		$this->db->select(array('COUNT(*) as total', 'SUM(subiu) as subiu'));
        $this->db->from('inscritos_novo');
		$this->db->where('id_evento', $id_evento);
		$dados = $this->db->get()->row();
		
		return array($dados->subiu, $dados->total);
	}
	
	// Contar Nova Inscrição
	public function countNova($id_evento) {
        $this->db->select(array('count(*) as total'));
        $this->db->from('inscritos_novo');
		$this->db->where('id_evento', $id_evento);
		
		return $this->db->get()->row()->total;
	}
	
	// Verificar se numero de peito já está cadastrado para outro atleta
	public function existNmPeito($id_evento, $nm_peito) {
		$numerosEncontrados = 0;
		
        $this->db->select(array('count(*) as total'));
        $this->db->from('inscritos');
		$this->db->where('id_evento', $id_evento);
		$this->db->where('nm_peito', $nm_peito);
		$numerosEncontrados += $this->db->get()->row()->total;
		
        $this->db->select(array('count(*) as total'));
        $this->db->from('inscritos_novo');
		$this->db->where('id_evento', $id_evento);
		$this->db->where('nm_peito', $nm_peito);
		$numerosEncontrados += $this->db->get()->row()->total;
		
		return $numerosEncontrados > 0;
	}
        
        // Verificar se numero de peito já está cadastrado para outro atleta
	public function existPedido($id_evento, $id_pedido) {
		$numerosEncontrados = 0;
		
        $this->db->select(array('count(*) as total'));
        $this->db->from('inscritos');
		$this->db->where('id_evento', $id_evento);
		$this->db->where('id_pedido', $id_pedido);
		$numerosEncontrados += $this->db->get()->row()->total;
		
		return $numerosEncontrados > 0;
	}
	
	// Pegar Numeros de Peito em Conflito
	public function getPeitoConflito($id_evento) {
        $this->db->select(array('cod_inscritos_novo',
								'nome_completo',
								'nm_peito'));
        $this->db->from('inscritos_novo');
		$this->db->where('conflito_nm_peito', '1');
		$this->db->where('id_evento', $id_evento);
		
		return $this->db->get()->result();
	}
	
	// Salvar novo numero de peito para os usuarios novos
	public function saveNmPeito($cod_inscritos_novo, $nm_peito) {
		$this->db->update('inscritos_novo', array('nm_peito' => $nm_peito, 'conflito_nm_peito' => false), array('cod_inscritos_novo' => $cod_inscritos_novo));
	}
        
        // Alterar inscritos
	public function updateInscritos($cod_inscritos, $data) {
		$this->db->update('inscritos', $data , array('cod_inscritos' => $cod_inscritos));
	}
        
        // Alterar usuario
	public function addUsuario($cod_usuario,$cod_inscritos, $data) {
            
            $data['dt_alterado'] = date('YmdHis');
            $data['cod_usuario'] = date('mdHis');
            $data['id_pedido_evento'] = $cod_inscritos;
            $usuario = $this->db->insert('usuario', $data);
            
            $this->db->update('inscritos', array('id_usuario'=>$data['cod_usuario']) , array('cod_inscritos' => $cod_inscritos));
            
            print_r($usuario);
            exit(0);
	}
        
        // Alterar usuario
	public function updateUsuario($cod_usuario, $data) {
		$this->db->update('usuario', $data , array('cod_usuario' => $cod_usuario));
	}
        
        // Get número de peito
	public function getUltimoNrPeito($id_modalidade) {
		$this->db->select(array('*'));
                $this->db->from('evento_modalidade');
                $this->db->where('cod_modalidade', $id_modalidade);
                return $this->db->get()->row();
	}
        
        // Get número de peito
	public function getUltimoNrPeitoPedido($id_modalidade) {
		$this->db->select(array('nm_peito'));
                $this->db->from('inscritos_novo');
                $this->db->where('modalidade', $id_modalidade);
                $this->db->order_by('cod_inscritos_novo', 'desc');
                $this->db->limit(1);
                return $this->db->get()->row();
	}
        
        // Get número de peito
	public function getUltimoNrPeitoPedidoExistente($id_evento) {
		$this->db->select(array('nm_peito'));
                $this->db->from('pedidos_existentes');
                $this->db->where('id_evento', $id_evento);
                $this->db->order_by('id_pedido_existente', 'desc');
                $this->db->limit(1);
                return $this->db->get()->row();
	}
        
        public function getListExport($id) {
            
            $this->db->select(array('pedidos_existentes.id_pedido_existente',
								'pedidos_existentes.id_pedido',
								'pedidos_existentes.nm_peito',
								'"" AS dt_pedido',
								'"" AS id_pedido_status',
								'"" AS form_pagamento',
								'"" AS id_comprador',
								'"" AS dt_pagamento',
								'pedidos_existentes.nome_completo AS nome',
								'pedidos_existentes.nr_documento AS documento',
								'"" as categoria',
								'"" as modalidade',
                                                                'tamanho_camiseta.nome AS camiseta',
								'"" as produtos',
                                                                '"Sim" as retirado',
                                                                'evento.nome AS evento',
								'pedidos_existentes.dt_alterado as retirado_data'));
        $this->db->from('pedidos_existentes');
        $this->db->join('evento', 'pedidos_existentes.id_evento = evento.cod_evento', 'left');
        $this->db->join('tamanho_camiseta', 'id_tamanho_camiseta = cod_tamanho_camiseta');
		$this->db->where('pedidos_existentes.id_evento', $id);
		$this->db->where('subiu',0);
		$this->db->order_by('id_pedido', 'asc');
		$this->db->group_by('pedidos_existentes.id_pedido');
		$query = $this->db->get_compiled_select();
            
            $this->db->select(array('inscritos.cod_inscritos',
								'inscritos.id_pedido',
								'inscritos.nm_peito',
								'inscritos.dt_pedido',
								'inscritos.id_pedido_status',
								'inscritos.form_pagamento',
								'inscritos.id_comprador',
								'inscritos.dt_pagamento',
								'usuario.nome',
								'usuario.documento',
								'evento_categoria.nome as categoria',
								'evento_modalidade.nome as modalidade',
                                                                'tamanho_camiseta.nome AS camiseta',
								'GROUP_CONCAT(ds_titulo SEPARATOR "<br>") as produtos',
								'IF((SELECT retirado
									 FROM retirado
									 WHERE cod_inscritos = id_inscritos
									 ORDER BY dt_alterado DESC
									 LIMIT 1)>0, "Sim", "Não") as retirado',
                                                                'evento.nome AS evento',
								'(SELECT dt_alterado
								  FROM retirado
								  WHERE cod_inscritos = id_inscritos
								  ORDER BY dt_alterado DESC
								  LIMIT 1) as retirado_data'));
        $this->db->from('inscritos');
		$this->db->join('usuario', 'cod_usuario = id_usuario', 'left');
                $this->db->join('evento', 'inscritos.id_evento = evento.cod_evento', 'left');
		$this->db->join('evento_categoria', 'cod_categoria = id_categoria', 'left');
                $this->db->join('tamanho_camiseta', 'id_tamanho_camiseta = cod_tamanho_camiseta');
		$this->db->join('evento_modalidade', 'cod_modalidade = id_modalidade', 'left');
		$this->db->join('inscritos_produto', 'inscritos_produto.id_pedido = inscritos.id_pedido', 'left');
		$this->db->where('inscritos.id_evento', $id);
		if(count($where) > 0) $this->db->where($where);
		$this->db->order_by('id_pedido', 'asc');
		$this->db->group_by('inscritos.cod_inscritos');
		$query1 = $this->db->get_compiled_select();
		
        $this->db->select(array('cod_inscritos_novo as cod_inscritos',
								'cod_inscritos_novo as id_pedido',
								'nm_peito',
								'inscritos_novo.dt_alterado as dt_pedido',
								'2 as id_pedido_status',
								'IF(forma_pagamento, "Dinheiro", "Cartão") as form_pagamento',
								'0 id_comprador',
								'inscritos_novo.dt_alterado as dt_pagamento',
								'nome_completo as nome',
								'v_nr_documento as documento',
								'evento_categoria.nome as categoria',
								'evento_modalidade.nome as modalidade',
                                                                'tamanho_camiseta.nome AS camiseta',
								'"" as produtos',
								'"Sim" as retirado',
                                                                'evento.nome AS evento',
								'inscritos_novo.dt_alterado as retirado_data'));
        $this->db->from('inscritos_novo');
		$this->db->join('evento_categoria', 'cod_categoria = categoria', 'left');
                $this->db->join('evento', 'inscritos_novo.id_evento = evento.cod_evento', 'left');
                $this->db->join('tamanho_camiseta', 'camiseta = cod_tamanho_camiseta');
		$this->db->join('evento_modalidade', 'cod_modalidade = modalidade', 'left');
		$this->db->where('id_evento', $id);
		if(count($where2) > 0) $this->db->where($where2);
		$query2 = $this->db->get_compiled_select();
		
		//if(count($where) > 0) $this->db->where($where);
		
		
		return $this->db->query('(' . $query . ') UNION ALL (' . $query1 . ') UNION ALL (' . $query2 . ')')->result();
	}
}