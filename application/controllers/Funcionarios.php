<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Funcionarios extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->checkLogin();
        $this->load->model('inscritos_model');
        $this->load->model('funcionarios_model');
		
		// Avisar qual o menu Current
		$this->menuCurrent['funcionarios'] = ' class="current"';
		$this->menuCurrent['home'] = '';
	}
	
	// Index
	public function index() {
		$data = array();
		$data['ACAO'] = 'adicionar';
		$data['id_evento_atual'] = $this->getEventoAtual()->id_evento;
		
		// Pegar e tratar a lista de funconarios
		$data['LIST'] = $this->funcionarios_model->getList();
		foreach($data['LIST'] as $name => $value) {
			$data['LIST'][$name]->administrador = (($value->administrador == 1)? 'Administrador' : 'Normal');
			$data['LIST'][$name]->ativo = (($value->ativo == 1)? 'Ativo' : 'Bloqueado');
		}
		
		$this->load( 'funcionarios/index', $data);
	}
	
	// Adicionar
	public function adicionar() {
		$data = array();
		$data['nome'] = $this->input->post( 'nome', true );
		$data['login'] = $this->input->post( 'login', true );
		$data['senha'] = md5($this->input->post( 'senha', true ));
		$data['email'] = $this->input->post( 'email', true );
		$data['administrador'] = $this->input->post( 'administrador', true );
		$data['ativo'] = $this->input->post( 'ativo', true );
		
		$this->funcionarios_model->insert($data);
		redirect('/funcionarios/');
		exit();
	}
	
	// Editar
	public function editar($id) {
		$data = array();
		$data['EDITAR'] = $this->funcionarios_model->get($id);
		$data['ACAO'] = 'edit/'.$id;
		
		$this->load( 'funcionarios/editar', $data);
	}
	
	// Adicionar
	public function edit($id) {
		$data = array();
		$data['nome'] = $this->input->post( 'nome', true );
		$data['login'] = $this->input->post( 'login', true );
		if($this->input->post( 'senha', true ) != ''
		&& $this->input->post( 'senha', true ) != '0') $data['senha'] = md5($this->input->post( 'senha', true ));
		$data['email'] = $this->input->post( 'email', true );
		$data['administrador'] = $this->input->post( 'administrador', true );
		$data['ativo'] = $this->input->post( 'ativo', true );
		
		$this->funcionarios_model->update($data, $id);
		redirect('/funcionarios/');
		exit();
	}
	
	// Remover
	public function remover($id) {
		$this->funcionarios_model->remover($id);
		redirect('/funcionarios/');
		exit();
	}
        
        // Limpar Base
	public function limpar() {
            // Avisar qual o menu Current
            $this->menuCurrent['limpar'] = ' class="current"';
            $this->menuCurrent['home'] = '';
            $this->menuCurrent['funcionarios'] = '';
            $this->load( 'funcionarios/limpar', $data);
	}
        
        // Limpar Base
	public function limpando() {
            $this->funcionarios_model->limpar();
            redirect('/');
            exit();
        }
}
