<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Eventos extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->checkLogin();
        $this->load->model('evento_model');
        $this->load->model('estoque_model');
        $this->load->model('retirada_model');
        $this->load->model('inscritos_model');
		
		// Avisar qual o menu Current
		$this->menuCurrent['eventos'] = ' class="current"';
		$this->menuCurrent['home'] = '';
	}
	
	// Index
	public function index() {
		$data = array();
		
		// Verificar se tem algum alert de erro
		if(isset($_GET['error']))
			$this->alert = $this->input->get('error');
		
		// Pegar o ID dos eventos já baixados
		$idCarregado = $this->evento_model->getIDEventos();
		
		// Pegar próximos eventos do Webservice
		$data['BAIXAR'] = $this->integracao->listNextEvents();
		if(count($data['BAIXAR']) > 0) {
			foreach($data['BAIXAR'] as $name => $value) {
				$data['BAIXAR'][$name]->dt_evento = date('d/m/Y', strtotime($value->dt_evento));
				
				if(in_array($value->id_evento, $idCarregado))
					unset($data['BAIXAR'][$name]);
			}
		}
		
		// Pegar ID do proximo evento
		$data['EVENTOS_ATUAL'] = array($this->getEventoAtual());
		$data['EVENTOS_ATUAL'][0]->dtevento = date('d/m/Y', strtotime($data['EVENTOS_ATUAL'][0]->dtevento));
		
		// Pegar informações para tabelas
		$data['EVENTOS_FUTUROS'] = $this->evento_model->getEventos(array('dtevento >=' => date('Y-m-d')));
		$data['EVENTOS_PASSADO'] = $this->evento_model->getEventos(array('dtevento <' => date('Y-m-d')));
		$data['LISTA_EVENTOS_ATUAL'] = $this->evento_model->getEventos(array('dtevento >= "' . date('Y-m-d') . "'"));
		
		foreach($data['EVENTOS_FUTUROS'] as $name => $value) {
$data['EVENTOS_FUTUROS'][$name]->dtevento = '<span style="display: none;">' . $value->dtevento . '</span>' . date('d/m/Y', strtotime($value->dtevento));
$data['EVENTOS_FUTUROS'][$name]->total = $this->inscritos_model->countInscricoes(array('id_evento' => $value->cod_evento));
$data['EVENTOS_FUTUROS'][$name]->retirados = $this->inscritos_model->countRetirados(array('id_evento' => $value->cod_evento));
$data['EVENTOS_FUTUROS'][$name]->porcentagem = 0;
if($data['EVENTOS_FUTUROS'][$name]->total > 0)
	$data['EVENTOS_FUTUROS'][$name]->porcentagem = round($data['EVENTOS_FUTUROS'][$name]->retirados * 100 / $data['EVENTOS_FUTUROS'][$name]->total, 2);
$pc1 = $this->retirada_model->getPCSubiu($value->cod_evento);
$pc2 = $this->inscritos_model->countPCNova($value->cod_evento);
$subiu = $pc1[0] + $pc2[0];
$total = $pc1[1] + $pc2[1];
$sincronizado = floor($subiu * 100 / $total);
if($sincronizado < 100)
	$data['EVENTOS_FUTUROS'][$name]->sincronizado = '<td class="falta_sincronizar">' . $sincronizado . ' %</td>';
else
	$data['EVENTOS_FUTUROS'][$name]->sincronizado = '<td class="sincronizado">' . $sincronizado . ' %</td>';
$data['EVENTOS_FUTUROS'][$name]->pedidos_novos = $this->inscritos_model->countNova($value->cod_evento);
		}
		
		foreach($data['EVENTOS_PASSADO'] as $name => $value) {
$data['EVENTOS_PASSADO'][$name]->dtevento = '<span style="display: none;">' . $value->dtevento . '</span>' . date('d/m/Y', strtotime($value->dtevento));
$data['EVENTOS_PASSADO'][$name]->total = $this->inscritos_model->countInscricoes(array('id_evento' => $value->cod_evento));
$data['EVENTOS_PASSADO'][$name]->retirados = $this->inscritos_model->countRetirados(array('id_evento' => $value->cod_evento));
$data['EVENTOS_PASSADO'][$name]->porcentagem = 0;
if($data['EVENTOS_PASSADO'][$name]->total > 0)
	$data['EVENTOS_PASSADO'][$name]->porcentagem = round($data['EVENTOS_PASSADO'][$name]->retirados * 100 / $data['EVENTOS_PASSADO'][$name]->total, 2);
$pc1 = $this->retirada_model->getPCSubiu($value->cod_evento);
$pc2 = $this->inscritos_model->countPCNova($value->cod_evento);
$subiu = $pc1[0] + $pc2[0];
$total = $pc1[1] + $pc2[1];
$sincronizado = floor($subiu * 100 / $total);
if($sincronizado < 100)
	$data['EVENTOS_PASSADO'][$name]->sincronizado = '<td class="falta_sincronizar">' . $sincronizado . ' %</td>';
else
	$data['EVENTOS_PASSADO'][$name]->sincronizado = '<td class="sincronizado">' . $sincronizado . ' %</td>';
$data['EVENTOS_PASSADO'][$name]->pedidos_novos = $this->inscritos_model->countNova($value->cod_evento);
		}
		
		$this->load( 'eventos/index', $data);
	}
	
	// Gerenciar o evento
	public function gerenciar($id) {
		$data = array();
		
		// Pegar dados
		$data['ID'] = $id;
		$data['TAMANHOS'] = $this->estoque_model->getTamanhos($id);
		$data['QUANTIDADE_LEVADA'] = $this->estoque_model->getQuantidadesLevadas($id);
		$data['QUANTIDADE_VENDIDA'] = $this->inscritos_model->countQuantidadeCamisetaVendida($id);
		$data['QUANTIDADE_RETIRADA'] = $this->inscritos_model->countQuantidadeCamisetaRetiradas($id);
		
		// Calcular quantidade de sobra
		$SOBRA = array();
		foreach($data['QUANTIDADE_LEVADA'] as $name => $value) {
			if($value->levados == 0) {
				unset($data['QUANTIDADE_LEVADA'][$name]);
				continue;
			}
			
			$SOBRA[$value->cod_tamanho_camiseta][0] = $value->levados;
			$SOBRA[$value->cod_tamanho_camiseta][2] = $value->nome;
		}
		foreach($data['QUANTIDADE_RETIRADA'] as $name => $value) {
			$SOBRA[$value->id_tamanho_camiseta][1] = $value->total;
			$SOBRA[$value->id_tamanho_camiseta][2] = $value->nome;
		}
		ksort($SOBRA);
		$data['QUANTIDADE_SOBRA'] = array();
		foreach($SOBRA as $name => $value)
			$data['QUANTIDADE_SOBRA'][] = array('total' => $value[0] - $value[1], 'nome' => $value[2]);
		
		// Pegar Produtos do evento
		$data['PRODUTOS'] = $this->inscritos_model->getCountProdutosEvento($id);
		
		$this->load( 'eventos/gerenciar', $data);
	}
	
	// Carregar evento
	public function carregar() {
                $this->load->model('retirada_model');
		// Verificar se foi passado o ID
		$id = intval($_GET['id_evento'] + $_POST['id_evento']);
		if($id < 1) {
			echo 'Erro ao selecionar o evento';
			exit();
		}
		
		// Carregar o Evento
		$integracao = $this->integracao->loadEvents($id, $this->retirada_model);
		
		// Verificar se deu algum problema de conexão
		if($integracao !== true)
			redirect('/eventos/?error=' . $integracao);
		else
			redirect('/eventos/');
	}
	
	// Enviar dados ao servidor
	public function enviar() {
		$this->load->model('funcionarios_model');
		$this->load->model('retirada_model');
		
		// Verificar se foi passado o ID
		$id = intval($_GET['id_evento'] + $_POST['id_evento']);
		if($id < 1) {
			echo 'Erro ao selecionar o evento';
			exit();
		}
		
		// Carregar o Evento
		$integracao = $this->integracao->uploadEvent($this->funcionarios_model, $this->retirada_model, $id);
		
		// Verificar se deu algum problema de conexão
		if($integracao !== true) {
			// Verificar se foi conflito de numero de peito
			if($integracao == 'CONFLITO')
				redirect('/eventos/conflito/' . $id);
			else
				redirect('/eventos/?error=' . $integracao);
		} else
			redirect('/eventos/');
	}
        
        // Enviar dados ao servidor
	public function export() {
		$this->load->model('inscritos_model');
		
		// Verificar se foi passado o ID
		$id = intval($_GET['id_evento'] + $_POST['id_evento']);
		if($id < 1) {
			echo 'Erro ao selecionar o evento';
			exit();
		}
                
                $data['dados'] = $this->inscritos_model->getListExport($id);
		
		$this->load( 'eventos/export', $data);
	}
	
	// Alterar evento atual
	public function atual() {
		// Verificar se foi passado o ID
		$id = intval($_GET['id_evento'] + $_POST['id_evento']);
		if($id < 1) {
			echo 'Erro ao selecionar o evento';
			exit();
		}
		
		// Carregar o Evento
		$this->evento_model->setEventoAtual($id);
		
		redirect('/eventos/');
	}
	
	// Gerenciar conflito nos numeros de peito
	public function conflito($id_evento) {
		$data = array();
		
		// Verificar se foi passado o ID
		$id_evento = intval($id_evento);
		if($id_evento == 0) redirect('/eventos/');
		$data['ID'] = $id_evento;
		
		// Selecionar Numeros de Peito em conflito
		$data['ATLETAS'] = $this->inscritos_model->getPeitoConflito($id_evento);
		
		// Mostrar tela
		$this->load( 'eventos/conflito_nr_peito', $data);
	}
	
	// Resolver conflitos dos numeros de peito
	public function resolver_conflito($id_evento) {
		// Salvar codigos novos de numero de peito
		if(count($_POST) > 0)
			foreach($_POST['nm_peito'] as $name => $value)
				$this->inscritos_model->saveNmPeito($name, $value);
		
		redirect('/eventos/');
	}
}
