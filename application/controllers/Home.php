<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->checkLogin();
        $this->load->model('login_model');
        $this->load->model('evento_model');
        $this->load->model('inscritos_model');
	}
	
	// Index
	public function index() {
		$data = array();
		
		// Carregar lista de eventos para o calendario
		$data['ULTIMOS7'] = $this->evento_model->getEventosHome();
		$data['CALENDARIO'] = $this->evento_model->getEventos();
		
		// Carregar numeros do ultimos 7
		foreach($data['ULTIMOS7'] as $name => $value) {
			$data['ULTIMOS7'][$name]->total = $this->inscritos_model->countInscricoes(array('id_evento' => $value->cod_evento));
			$data['ULTIMOS7'][$name]->entregue = $this->inscritos_model->countRetirados(array('id_evento'=>$value->cod_evento));
			$data['ULTIMOS7'][$name]->sobrou = $data['ULTIMOS7'][$name]->total - $data['ULTIMOS7'][$name]->entregue;
			$data['ULTIMOS7'][$name]->nome = substr($data['ULTIMOS7'][$name]->nome, 0, 25);
		}
		
		// Pegar ultimos usuarios que logaram
		$data['ULTIMOS_LOGINS'] = $this->login_model->ultimosLoginLog();
		foreach($data['ULTIMOS_LOGINS'] as $name => $value) {
			// Verificar se tem avatar
			if($data['ULTIMOS_LOGINS'][$name]->avatar == '')
				$data['ULTIMOS_LOGINS'][$name]->avatar = base_url() . 'assets/images/thumbs/no-user.png';
			$data['ULTIMOS_LOGINS'][$name]->data = date('d/m/Y H:i:s', strtotime($data['ULTIMOS_LOGINS'][$name]->data));
		}
		
		$this->load( 'home' , $data );
	}
}
