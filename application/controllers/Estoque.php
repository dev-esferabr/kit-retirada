<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Estoque extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->checkLogin();
		$this->load->model('estoque_model');
	}
	
	// Index
	public function adicionar($id) {
		$this->estoque_model->insert($id,
									 $this->input->post( 'cod_tamanho_camiseta', true ),
									 $this->input->post( 'total', true ));
		redirect('/eventos/gerenciar/' . $id);
		exit();
	}
	
	// Index
	public function remover($id, $id2) {
		$this->estoque_model->remover($id, $id2);
		redirect('/eventos/gerenciar/' . $id);
		exit();
	}
}
