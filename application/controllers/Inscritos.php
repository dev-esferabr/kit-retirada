<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Inscritos extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->checkLogin();
        $this->load->model('inscritos_model');
		
		// Avisar qual o menu Current
		$this->menuCurrent['inscritos'] = ' class="current"';
		$this->menuCurrent['home'] = '';
	}
	
	// Index
	public function index() {
		$data = array();
		$data['id_evento_atual'] = $this->getEventoAtual()->id_evento;
		$data['evento_nome'] = $this->getEventoAtual()->nome;
		
		// Pegar Informações para as novas incrições
		$data['MODALIDADE'] = $this->inscritos_model->getModalidade($this->getEventoAtual()->id_evento);
		$data['CATEGORIA'] = $this->inscritos_model->getCategoria($this->getEventoAtual()->id_evento);
		$data['CAMISETA'] = $this->inscritos_model->countQuantidadeCamisetaVendida($this->getEventoAtual()->id_evento);
                
                $data['ID_EVENTO'] = $this->getEventoAtual()->id_evento;
		
		$this->load( 'inscritos/index', $data);
	}
	
	// Index
	public function getList($id) {
		// Verificar se tem busca
		$where = array();
		if($this->input->get('search_tipo') != ''
		&& $this->input->get('search_atleta') != '') {
			switch($this->input->get('search_tipo')) {
				case '0':
					$where['inscritos.id_pedido'] = ((int) $this->input->get('search_atleta'));
				break;
				case '1':
					$where['usuario.documento'] = $this->input->get('search_atleta');
				break;
				case '2':
					$where['inscritos.nm_peito'] = $this->input->get('search_atleta');
				break;
				case '3':
					$where['usuario.email'] = $this->input->get('search_atleta');
				break;
				case '4':
					$where['usuario.celular LIKE'] = '%' . $this->input->get('search_atleta') . '%';
				break;
				case '5':
					$where['usuario.telefone LIKE'] = '%' . $this->input->get('search_atleta') . '%';
				break;
				case '6':
					$where['usuario.equipe LIKE'] = '%' . $this->input->get('search_atleta') . '%';
				break;
				case '7':
					$where['usuario.nome LIKE'] = '%' . $this->input->get('search_atleta') . '%';
				break;
			}
		}
		
		// Verificar se tem busca2
		$where2 = array();
		if($this->input->get('search_tipo') != ''
		&& $this->input->get('search_atleta') != '') {
			switch($this->input->get('search_tipo')) {
				case '0':
					$where2['cod_inscritos_novo LIKE '] = $this->input->get('search_atleta') . '%';
				break;
				case '1':
					$where2['v_nr_documento LIKE'] = $this->input->get('search_atleta') . '%';
				break;
				case '2':
					$where2['nm_peito'] = $this->input->get('search_atleta');
				break;
				case '3':
					$where2['email LIKE'] = $this->input->get('search_atleta') . '%';
				break;
				case '4':
					$where2['celular LIKE'] = '%' . $this->input->get('search_atleta') . '%';
				break;
				case '5':
					$where2['telefone LIKE'] = '%' . $this->input->get('search_atleta') . '%';
				break;
				case '6':
					$where2['equipe LIKE'] = '%' . $this->input->get('search_atleta') . '%';
				break;
				case '7':
					$where2['nome_completo LIKE'] = '%' . $this->input->get('search_atleta') . '%';
				break;
			}
		}
		// Pegar lista
		$data = array();
		$data['aaData'] = array();
		$lista = $this->inscritos_model->getList($id, $where, $where2);
		foreach($lista as $name => $value){
                    $existente = 'normal';
                    if($value->id_comprador == 0){
                        $existente = 'existente';
                    }
			$data['aaData'][] = array(	'DT_RowId' => 'id_' . $value->id_pedido,'DT_RowClass'=>$existente,
										'0' => $value->id_pedido,
										'1' => $value->nome,
										'2' => $value->nm_peito,
                                                                                '3' => (($value->id_pedido_status == 2)? 'Pago': 'Não confirmado'),
										'4' => $value->modalidade,
										'5' => $value->categoria,
                                                                                '6' => $value->camiseta,
                                                                                '7' => $value->fl_capitao,
										'8' => $value->documento,
										'9' => $value->produtos,
                                                                                '10' => (($value->retirado == 'Não' && ($value->id_pedido_status == 2 || $this->nativesession->userdata('logged_in')->administrador == 1))? '<input type="checkbox" name="retirada_incricoes['.$value->cod_inscritos.']" data-pedido="'.$value->id_pedido.'" data-comprador="'.$value->id_comprador.'" data-nome_comprador="'.$value->nome_comprador.'" data-telefone_comprador="'.$value->telefone_comprador.'" value="1"/>': ' '),
										'11' => (($value->retirado_data)? '<a href="#" class="retirado_sim" data-id="' . $value->cod_inscritos . '">' . $value->retirado . '</a>': 'Não'),
										'12' => (($value->retirado_data)? '<span style="display:none;">' . $value->retirado_data . '</span>' . date('d/m/Y H:i:s', strtotime($value->retirado_data)):'-'));
                }

		echo json_encode($data);
		exit();
	}
	
	// Index
	public function detalhe($id_evento, $id) {
            $this->load->model('retirada_model');
            $this->load->model('inscritos_model');
		
		// Verificar se é uma inscrição nova
		if(strlen($id) > 10) {
			$this->detalheNovo($id_evento, $id);
			return true;
		}
		
		// Pegar lista de inscritos no pedido
		$dadosPedido = $this->inscritos_model->getListByPedido($id_evento, $id);
		
		// Pegar dados do comprador
		$comprador = $this->inscritos_model->getUsuarioByID($dadosPedido[0]->id_comprador,$dadosPedido[0]->fl_balcao);
		
		// Dados do pedido
		$data = array();
		$data['ID'] = $id;
		$data['DT_PEDIDO'] = date('d/m/Y H:i:s', strtotime($dadosPedido[0]->dt_pedido));
		$data['STATUS'] = (($dadosPedido[0]->id_pedido_status == 2)? 'Pago': 'Não confirmado');
		$data['PAGAMENTO'] = $dadosPedido[0]->form_pagamento;
		$data['COMPRADOR'] = $comprador->nome;
                $data['DOCUMENTO'] = $comprador->documento;
                $data['BALCAO'] = $dadosPedido[0]->fl_balcao;
		$data['TELEFONE'] = $comprador->telefone;
		$data['DT_PAGAMENTO'] = date('d/m/Y H:i:s', strtotime($dadosPedido[0]->dt_pagamento));
                
                $modalidades = $this->inscritos_model->getModalidade($id_evento);
		$categorias = $this->inscritos_model->getCategoria($id_evento);
                
		// Lista de Atletas
		$data['ATLETAS'] = $dadosPedido;
		foreach($data['ATLETAS'] as $name => $value) {
                        $modalidades = $this->inscritos_model->getModalidade($id_evento);
                        $categorias = $this->inscritos_model->getCategoria($id_evento);
                        
			$data['ATLETAS'][$name]->retiradoCheck = (($data['ATLETAS'][$name]->retirado == 'Sim')? ' checked' : '');
                        $data['ATLETAS'][$name]->disabled = (($data['ATLETAS'][$name]->retirado == 'Sim')? ' disabled' : '');
                        $data['retirado'] = (($data['ATLETAS'][$name]->retirado == 'Sim')? ' checked' : '');
                        $data['id_pedido_status'] = $value->id_pedido_status;
                        $data['ATLETAS'][$name]->MODALIDADE = $modalidades;
                        $data['ATLETAS'][$name]->CATEGORIA = $categorias;
                        $data['ATLETAS'][$name]->fl_capitao = (($value->fl_capitao == 1)? 'Sim' : 'Não');
                        
                        foreach ($data['ATLETAS'][$name]->MODALIDADE as $key => $mod){
                            if($mod->cod_modalidade == $value->id_modalidade){
                               $data['ATLETAS'][$name]->MODALIDADE[$key]->selected = 'selected';
                            }else{
                                $data['ATLETAS'][$name]->MODALIDADE[$key]->selected = '';
                            }
                        }
                         
                        foreach ($value->CATEGORIA as $key => $cat){
                            if($cat->cod_categoria == $value->id_categoria){
                               $data['ATLETAS'][$name]->CATEGORIA[$key]->selected = 'selected';
                            }else{
                                $data['ATLETAS'][$name]->CATEGORIA[$key]->selected = '';
                            }
                        }
		}
		
		// Produtos compradores
		$data['PRODUTOS'] = $this->inscritos_model->getCountProdutosEvento($id_evento, $id);
		foreach($data['PRODUTOS'] as $name => $value) {
			$data['PRODUTOS'][$name]->retirado = (($data['PRODUTOS'][$name]->retirado > 0)? 'Sim': 'Não');
			$data['PRODUTOS'][$name]->retiradoCheck = (($data['PRODUTOS'][$name]->retirado == 'Sim')? ' checked' : '');
		}
		
		// Dados Estras
		$data['ID_COMPRADOR'] = $dadosPedido[0]->id_comprador;
		$data['ID_EVENTO'] = $id_evento;
		
		// Pegar histórico do pedido
		$data['HISTORICO'] = $this->retirada_model->getHistoricoPedido($id);
                
		$this->load( 'inscritos/detalhe', $data);
	}
        
        public function alterarusuario(){
            $params = $_GET;
            
            $dtnascimento = explode('/', $params['dtnascimento']);
            
            if($params['cod_inscritos'] > 0){
                $dados = array(
                    'nome'=>$params['nome'],
                    'dtnascimento'=> $dtnascimento[2].'-'.$dtnascimento[1].'-'.$dtnascimento[0],
                    'telefone'=>$params['telefone'],
                    'celular'=>$params['celular'],
                    'email'=>$params['email'],
                    'documento'=>$params['documento'],
                    'genero'=>$params['genero'],
                    'necessidades'=>$params['necessidades'],
                    'fl_alterado'=>1
                );
                $this->inscritos_model->addUsuario($params['cod_usuario'],$params['cod_inscritos'],$dados);
            }else{
                $dados = array(
                    'nome'=>$params['nome'],
                    'dtnascimento'=> $dtnascimento[2].'-'.$dtnascimento[1].'-'.$dtnascimento[0],
                    'telefone'=>$params['telefone'],
                    'celular'=>$params['celular'],
                    'genero'=>$params['genero'],
                    'necessidades'=>$params['necessidades'],
                    'fl_alterado'=>1
                );
                $this->inscritos_model->updateUsuario($params['cod_usuario'],$dados);
            }
            exit(0);
        }
        
        public function alterar(){
            $params = json_decode($_POST['dados']);
            foreach($params as $insc){
                $dados = array(
                    'id_modalidade'=>$insc->id_modalidade,
                    'id_categoria'=>$insc->id_categoria,
                    'id_usuario'=>$insc->id_usuario,
                    'fl_alterado'=>1
                );
                $this->inscritos_model->updateInscritos($insc->cod_inscritos,$dados);
            }
            
            exit(0);
        }
        
        public function gerarnrpeito(){
            
            $id_modalidade = $_POST['id_modalidade'];
            
            $modalidade = $this->inscritos_model->getUltimoNrPeito($id_modalidade);
            $inscritos_novo = $this->inscritos_model->getUltimoNrPeitoPedido($id_modalidade);
            //$pedidos_existente = $this->inscritos_model->getUltimoNrPeitoPedidoExistente($modalidade->id_evento);
            
            if(count($inscritos_novo) <= 0){
                $nr_peito = $modalidade->nr_peito_extra_de;
            }else{
                $nr_peito = $inscritos_novo->nm_peito+1;
            }
            
            if($nr_peito >= $modalidade->nr_peito_extra_ate){
                $nr_peito = -1;
            }
            echo $nr_peito;
            //print_r($modalidade);
            //print_r($inscritos_novo);
            
            exit(0);
        }
	
	// Detalhe de Novas inscricoes
	public function detalheNovo($id_evento, $id) {
		$this->load->model('funcionarios_model');
		
		// Pegar lista de inscritos no pedido
		$dadosPedido = $this->inscritos_model->getPedidoNovoByID($id_evento, $id);
		
		// Dados do pedido
		$data = array();
		$data['ID'] = $id;
		$data['DT_PEDIDO'] = date('d/m/Y H:i:s', strtotime($dadosPedido->dt_alterado));
		$data['STATUS'] = 'Pago';
		$data['PAGAMENTO'] = (($dadosPedido->forma_pagamento)? "Dinheiro": "Cartão");
		$data['COMPRADOR'] = $dadosPedido->nome_completo;
		$data['TELEFONE'] = $dadosPedido->telefone;
		$data['DT_PAGAMENTO'] = date('d/m/Y H:i:s', strtotime($dadosPedido->dt_alterado));
		
		// Lista de Atletas
		$data['ATLETAS'] = array(array( 'cod_inscritos' => $id,
										'nome' => $dadosPedido->nome_completo,
										'nm_peito' => $dadosPedido->nm_peito,
										'modalidade' => $this->inscritos_model->getModalidadeByID($dadosPedido->modalidade)->nome,
										'categoria' => $this->inscritos_model->getCategoriaByID($dadosPedido->categoria)->nome,
										'camiseta' => $this->inscritos_model->getCamisetaByID($dadosPedido->camiseta)->nome,
										'retirado' => 'Sim'));
		
		// Produtos compradores
		$data['PRODUTOS'] = array();
		
		// Dados Estras
		$data['ID_COMPRADOR'] = -1;
		$data['ID_EVENTO'] = $id_evento;
		
		// Pegar histórico do pedido
		$data['HISTORICO'] = array(array('dt_alterado' => $dadosPedido->dt_alterado,
										 'funcionario' => end($this->funcionarios_model->get($dadosPedido->cod_funcionario))->nome,
										 'nome' => $dadosPedido->nome_completo,
										 'telefone' => $dadosPedido->telefone,
										 'obs' => '',
										 'acao' => 'Nova inscrição comprada e retirada.' ));
		
		$this->load( 'inscritos/detalhe', $data);
	}
	
	// Marcar como retirado
	public function retirado($id_evento, $id_pedido) {
        $this->load->model('retirada_model');
		
		// Pegar Post
		$inscricoes = $this->input->post('retirada_incricoes');
		$produtos = $this->input->post('retirada_produtos');
		$salvar = array();
		
		// Salvar lista de pedidos
		foreach($inscricoes as $name => $value) {
			$aux = intval($this->retirada_model->getRetiradaByIten($name, 0)->retirado);
			if($value != $aux) $salvar[] = array($name, 0, $value);
		}
		
		// Salvar lista de produtos
		foreach($produtos as $name => $value) {
			$aux = intval(@$this->retirada_model->getRetiradaByIten(0, $name)->retirado);
			if($value != $aux) $salvar[] = array(0, $name, $value);
		}
		
		// Salvar dados da retirada
		$data = array();
		$data['eh_comprador'] = $this->input->post('eh_comprador');
		$data['nome'] = $this->input->post('nome_retirado');
		$data['telefone'] = $this->input->post('telefone_retirado');
		$data['obs'] = $this->input->post('obs');
		$data['cod_funcionario'] = $this->nativesession->userdata('logged_in')->cod_funcionario;
		$data['id_pedido'] = $id_pedido;
		$cod_retirado_info = $this->retirada_model->saveInfo($data);
		
		// Salvar Retirada
		foreach($salvar as $name => $value) {
			$data = array();
			$data['cod_retirado_info'] = $cod_retirado_info;
			$data['id_inscritos'] = $value[0];
			$data['id_inscritos_produto'] = $value[1];
			$data['id_evento'] = $id_evento;
			$data['retirado'] = $value[2];
			$this->retirada_model->save($data);
		}
		
		redirect('/inscritos/');
	}
	
	// Pegar lista de cidades
	public function get_cidade($id) {
		$listaCidade = $this->inscritos_model->getCidade($id);
		echo '<select name="id_cidade" id="id_cidade" required="required">';
		foreach($listaCidade as $name => $value)
			echo '<option value="' . $value->id_cidade . '">' . $value->ds_cidade . '</option>';
		echo '</select>';
	}
	
	// Adicionar nova inscrição
	public function nova() {
		
		// Pegar dados para salvar nova inscrição
		$data = array();
		$data['id_evento'] = $this->getEventoAtual()->id_evento;
		$data['cod_funcionario'] = $this->nativesession->userdata('logged_in')->cod_funcionario;
		$data['email'] = $this->input->post('email');
		$data['nome_completo'] = $this->input->post('nome_completo');
		$data['nm_peito'] = $this->input->post('nm_peito');
		$data['h_tipo_cpf'] = $this->input->post('h_tipo_cpf');
		$data['v_nr_documento'] = $this->input->post('v_nr_documento');
		$data['nascimento1'] = $this->input->post('nascimento1');
		$data['nascimento2'] = $this->input->post('nascimento2');
		$data['nascimento3'] = $this->input->post('nascimento3');
		$data['genero'] = $this->input->post('genero');
		$data['id_estado'] = $this->input->post('id_estado');
		$data['id_cidade'] = $this->input->post('id_cidade');
		$data['telefone'] = $this->input->post('telefone');
		$data['celular'] = $this->input->post('celular');
		$data['modalidade'] = $this->input->post('modalidade');
		$data['categoria'] = $this->input->post('categoria');
		$data['camiseta'] = $this->input->post('camiseta');
		$data['forma_pagamento'] = $this->input->post('forma_pagamento');
		
		// Salvar Nova Inscrição
		$newID = $this->inscritos_model->nova($data);
		
		redirect('/inscritos/detalhe/' . $this->getEventoAtual()->id_evento . '/' . $newID);
	}
        
        // Adicionar inscrição existente
	public function existente() {
		
		// Pegar dados para salvar nova inscrição
		$data = array();
		$data['id_evento'] = $this->getEventoAtual()->id_evento;
		$data['cod_funcionario'] = $this->nativesession->userdata('logged_in')->cod_funcionario;
		$data['email'] = $this->input->post('email');
		$data['nome_completo'] = $this->input->post('nome_completo');
		$data['nm_peito'] = $this->input->post('nm_peito');
                $data['id_pedido'] = $this->input->post('id_pedido');
                $data['id_tamanho_camiseta'] = $this->input->post('camiseta');
		
		// Salvar Nova Inscrição
		$newID = $this->inscritos_model->existente($data);
		
		redirect('/inscritos/');
	}
	
	// Mostrar dados do atleta
	public function atleta($id,$cod_inscritos) {
		$this->displayTemplate = false;
		
		// Pegar Dados do Atleta
		$data = (array) $this->inscritos_model->getByID($id);
		$data = array_merge((array) $this->inscritos_model->getUsuarioByID($data['id_usuario'],$data['fl_balcao']), $data);
		
		// Arrumar dados para a exibição
		$data['dtnascimento'] = date('d/m/Y', strtotime($data['dtnascimento']));
		$data['modalidade'] = $this->inscritos_model->getModalidadeByID($data['id_modalidade'])->nome;
		$data['categoria'] = $this->inscritos_model->getCategoriaByID($data['id_categoria'])->nome;
		$data['camiseta'] = $this->inscritos_model->getCamisetaByID($data['id_tamanho_camiseta'])->nome;
                $data['cod_inscritos'] = $cod_inscritos;
		
		$this->load( 'inscritos/atleta', $data );
	}
	
	// Mostrar dados do atleta
	public function historico($id_inscritos = 0, $id_produto = 0) {
		$this->displayTemplate = false;
		$this->load->model('retirada_model');
		
		// Verificar se é uma nova inscricao
		if(strlen($id_inscritos) > 10) {
			$this->historicoNovo($id_inscritos);
			return true;
		}
		
		// Carregar Histórico do Produto
		$data = (array) $this->retirada_model->getUltimoHistoricoProduto($id_inscritos, $id_produto);
		$data['dt_alterado'] = date('d/m/Y H:i:s', strtotime($data['dt_alterado']));
		$data['obs'] = nl2br($data['obs']);
		
		$this->load( 'inscritos/historico', $data );
	}
	
	// Mostrar dados do atleta
	public function historicoNovo($id_inscritos) {
		// Carregar Histórico do Produto
		$data = (array) $this->inscritos_model->getPedidoNovoByID(0, $id_inscritos);
		$data['dt_alterado'] = date('d/m/Y H:i:s', strtotime($data['dt_alterado']));
		$data['nome'] = $data['nome_completo'];
		$data['obs'] = 'Nova inscrição comprada e retirada.';
		
		$this->load( 'inscritos/historico', $data );
	}
	
	// Verificar se já existe algum usuarios com o numero de peito escolhido
	public function nm_peito_repetido($id_evento, $nm_peito) {
		if($this->inscritos_model->existNmPeito($id_evento, $nm_peito))
			echo 'SIM';
		else
			echo 'NAO';
		exit();
	}
        
        // Verificar se já existe algum usuarios com o numero de peito escolhido
	public function pedido_repetido($id_evento, $id_pedido) {
		if($this->inscritos_model->existPedido($id_evento, $id_pedido))
			echo 'SIM';
		else
			echo 'NAO';
		exit();
	}
}
