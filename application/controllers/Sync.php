<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Sync extends MY_Controller {
	public $pastaBase = false;
	
	public function __construct() {
		parent::__construct();
		$this->load->model('sync_model');
		$this->pastaBase = $this->sync_model->pastaBase;
		
		// Aumentar o PHP Time desta pagina
		ini_set('max_execution_time', 0);
		ini_set('memory_limit', '2048M');
		set_time_limit(0);
	}
	
	// Retorna se este computador é um servidor
	public function isServer() {
		if($this->sync_model->isServer() && (@$_GET['unico'] != 'true' || $_SERVER['SERVER_ADDR'] != $_SERVER['REMOTE_ADDR']))
			$return = 'true';
		else
			$return = 'false';
		
		if(isset($_GET['callback'])) {
			header('Content-Type: application/json');
			echo $_GET['callback'] . '(' . $return . ')';
		} else
			echo $return;
	}
	
	// Procurar Servidor no modo Fast
	public function findServer() {
		$this->findServerTime(10);
	}
	
	// Procurar Servidor no modo Fast
	public function findFast() {
		$this->findServerTime(1);
	}
	
	// Procurar Servidor
	public function findServerTime($tempo) {
		$_SESSION['SERVERIP'] = false;
		session_write_close();
		
		// Abrir treads para procurar o server
		for($i=0; $i<16; $i++)
			$this->abrirTread(base_url() . 'sync/find_server_by_session/' . $i * 16 . '/' . $tempo . '?unico=' . @$_GET['unico']);
		
		// Dividir busca de servidor
		for($i=0; $i<50; $i++) {
			usleep(100000 * $tempo);
			
			session_start();
			if($_SESSION['SERVERIP'] !== false) {
				echo $_SESSION['SERVERIP'];
				exit();
			}
			session_write_close();
		}
		echo 'false';
	}
	
	// Fazer Curl em outro IP
	public function abrirTread($url) {
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_HEADER, false);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT_MS, 1000);
		curl_setopt($ch, CURLOPT_COOKIE, 'PHPSESSID=' . $_COOKIE['PHPSESSID']); 
		curl_setopt($ch, CURLOPT_TIMEOUT_MS, 10);
		$response = curl_exec($ch);
		curl_close($ch);
		
		return $response;
	}
	
	// Procurar Server By Session
	public function find_server_by_session($iInicial, $tempo) {
		session_write_close();
		
		for($i = $iInicial; $i<$iInicial + 16; $i++) {
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL, 'http://192.168.0.' . $i . $this->pastaBase . '/sync/isServer?unico=' . @$_GET['unico']);
			curl_setopt($ch, CURLOPT_HEADER, false);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($ch, CURLOPT_CONNECTTIMEOUT_MS, 100 * $tempo); 
			curl_setopt($ch, CURLOPT_TIMEOUT_MS, 0);
			$response = curl_exec($ch);
			if($response == 'true') {
				session_start();
				$_SESSION['SERVERIP'] = '192.168.0.' . $i;
				exit();
			}
			curl_close($ch);
		}
	}
	
	// Transformar esta estação em servidor
	public function central() {
		$this->displayHead = false;
		$this->load( 'sync/central', array());
	}
	
	// Tranformar essa estação na nova central
	public function nova_central() {
		$this->sync_model->thisServer();
		echo '<script>parent.postMessage("pm_kit_reload();","*");</script>';
		exit();
	}
	
	// Dois servidores
	public function duplicado() {
		$this->displayHead = false;
		$this->load( 'sync/duplicado', array());
	}
	
	// Tranformar essa estação na nova central
	public function nova_estacao() {
		$this->sync_model->thisNotServer();
		echo '<script>parent.postMessage("pm_kit_reload();","*");</script>';
		exit();
	}
	
	// Avisar que a Internet Esta Ligada/Desligada
	public function on_off($ligada) {
		$this->load->library('integracao');
		$this->integracao->onOff((($ligada == 'S')?'S':'N'));
	}
	
	// Carregar dados Full
	public function sincronizar($central) {
		$dataUltimoSync = $this->nativesession->userdata('sync_date');
		$this->nativesession->set_userdata('sync_date', $this->sync_model->curl('exportDate', $central));
		if(strlen($dataUltimoSync) < 6)
			$this->getFullServerData($central);
		else
			$this->getDataByDate($central, $dataUltimoSync);
	}
	
	// Carregar dados Full
	public function sincronizeme() {
		$this->getFullServerData($_SERVER['REMOTE_ADDR']);
	}
	
	###################################################################### Funções de Exports
	// Exporta os dados do sersvidor atual
	public function exportDate() {
		echo date('Y-m-d H:i:s');
	}
	
	// Exporta os dados do servidor atual
	public function exportFullData() {
		echo gzcompress(json_encode($this->sync_model->getFullData()));
	}
	
	// Exporta os dados do servidor atual pelos IDs
	public function exportDataByID() {
		echo gzcompress(json_encode($this->sync_model->exportDataByID(json_decode(gzuncompress(base64_decode($this->input->post('data'))), true))));
	}
	
	// Exporta os dados do servidor atual pelos IDs
	public function exportDataByDate() {
		echo gzcompress(json_encode($this->sync_model->exportDataByDate($this->input->post('data'))));
	}
	
	######################################################################
	// Pegar dados do servidor
	public function getFullServerData($IP) {
		// Varrer dados para ver se tem algum diferente
		$ids = $this->sync_model->checkFullData(json_decode(gzuncompress($this->sync_model->curl('exportFullData', $IP)),true));
		
		// Pegar dados que devem ser sincronizados
		$this->getDataByID($IP, $ids);
		
		return 'OK';
	}
	
	// Pegar dados que estão faltando
	public function getDataByID($IP, $ids) {
		// Pegar Dados Pelo ID
		$data = $this->sync_model->curl('exportDataByID', $IP, array('data' => base64_encode(gzcompress(json_encode($ids)))));
		
		// Salvar dados pegos
		$this->sync_model->exportSave(json_decode(gzuncompress($data), true));
		
		echo 'OK';
		exit();
	}
	
	// Pegar dados que estão faltando
	public function getDataByDate($IP, $date) {
		// Pegar Dados Pelo ID
		$data = $this->sync_model->curl('exportDataByDate', $IP, array('data' => $date));
		
		// Salvar dados pegos
		$this->sync_model->exportSave(json_decode(gzuncompress($data), true));
		
		echo 'OK2';
		exit();
	}
}
