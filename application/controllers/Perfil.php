<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Perfil extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->checkLogin();
	}
	
	// Index
	public function editar() {
		$data = array();
		$this->load( 'perfil/editar', $data);
	}
}
