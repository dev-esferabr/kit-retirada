<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends MY_Controller {

	public function __construct() {
		parent::__construct();
		$this->load->model('login_model');
	}
	
	// Index
	public function index($tipo = 0) {
		$data = array();
		
		// Verificar se deu erro
		if($_GET['error'] == 'true')
			$data['ERROR'] = ' style="display: block;"';
		
		// Pegar o tipo do login
		$data['TIPO_LOGIN'] = (($tipo == 1)? 1: 0);
		
		$this->displayTemplate = false;
		$this->load( 'login', $data);
	}
	
	// Logar
	public function logar() {
		// Acertar o tipo do login
		$_POST['TIPO_LOGIN'] = (($_POST['TIPO_LOGIN'] == 1)? 1: 0);
		
		// Selecionar dados
		$result = $this->login_model->login($this->input->post( 'username', true ),
											$this->input->post( 'password', true ),
											$this->input->post( 'TIPO_LOGIN', true ));
		
		// Verificar se foi encontrado o usuario
		if(count($result) > 0) {
			// Salvar log do Login
			$this->login_model->saveLog($result[0]->cod_funcionario);
			
			// Verificar se tem avatar
			if($result[0]->avatar == '')
				$result[0]->avatar = base_url() . 'assets/images/thumbs/no-user.png';
			
			// Verificar tipo de acesso
			if($this->input->post( 'TIPO_LOGIN', true ) == 1
			&& $result[0]->administrador == 1)
				$result[0]->NIVEL = 1;
			else
				$result[0]->NIVEL = 0;
			
			// Salvar e entrar no sistema
			$this->nativesession->set_userdata('logged_in', $result[0]);
			
			// Verifcar se tem permissão para ver a home
			if($this->hasPermission(1))
				redirect('/');
			else
				redirect('/inscritos');
		} else
			redirect('/login/' . $this->input->post( 'TIPO_LOGIN', true ) . '/?error=true');
		
		exit();
	}
	
	// DesLogar
	public function deslogar() {
		$this->nativesession->unset_userdata('logged_in');
		//redirect('/login/');
		echo '<script>parent.postMessage("pm_kit_close();","*");</script>';
		exit();
	}
}
