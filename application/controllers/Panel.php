<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Panel extends MY_Controller {
	
	public function __construct() {
		parent::__construct();
		$this->load->model('sync_model');
		$this->pastaBase = $this->sync_model->pastaBase;
	}
	
	// Mostar o Iframe da Central
	public function central() {
		$data = array();
		$data['NIVEL'] = 1;
		$data['tipo'] = 'central';
		$data['pastaBase'] = $this->pastaBase;
		$data['thisServer'] = $this->sync_model->isServer();
		$this->displayHead = false;
		$this->load( 'panel/iframe', $data);
	}
	
	// Mostar o Iframe da Retirada
	public function retirada() {
		$data = array();
		$data['NIVEL'] = 0;
		$data['tipo'] = 'retirada';
		$data['pastaBase'] = $this->pastaBase;
		$data['thisServer'] = $this->sync_model->isServer();
		$this->displayHead = false;
		$this->load( 'panel/iframe', $data);
	}
	
	// Mostrar Gif de carregando
	public function carregando() {
		$this->displayHead = false;
		$this->load( 'panel/loading', array());
	}
}